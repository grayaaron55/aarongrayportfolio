let vidStitch = require('video-stitch');
let vidCut = vidStitch.cut;
let vidMerge = vidStitch.merge;
let vidConcat = vidStitch.concat;

//Sandbox : )

let relativeVidPath = `../media/video/downloaded`
let absoluteVidPath = `/Users/pixelluminati/desktop/algoclass/kotakubot/media/video/downloaded`;
let testEpisode = `32_4186`;

var testCut = () => {
    let relativeTestVidPath = `${relativeVidPath}/${testEpisode}/Jazzpunk/Jazzpunk_DrZqGO6iBb8.mp4`;
    let absoluteTestVidPath = `${absoluteVidPath}/${testEpisode}/Jazzpunk/Jazzpunk_DrZqGO6iBb8.mp4`;
    var tstVidDuration = timestampFromSeconds((3*60)+2);

    var durat = durationFromInfo({
        'h':0,
        'm':'2',
        's':'24'
    })

    vidCut({
        silent: false
    })
    .original({
        "fileName": relativeTestVidPath,
        "duration": tstVidDuration
    })
    .exclude([{
        "startTime": timestampFromSeconds(0),
        "duration": timestampFromSeconds(30)
    },
    {
        "startTime": timestampFromSeconds(60),
        "duration": (tstVidDuration-60)
    }])
    .cut()
    .then(info => {
        console.log('in THEN')
        console.log(info)
    })
    .catch(err => {
        console.log('in CATCH')
        console.log(err['err'])
    })
}

var durationFromInfo = (info) => {
    var h = info['h']==0 ? 0 : parseInt(info['h'], 10);
    var m = info['m']==0 ? 0 : parseInt(info['m'], 10);
    var s = info['s']==0 ? 0 : parseInt(info['s'], 10);

    console.log(`dur from info: ${info['h']}+${info['m']}+${info['s']} :: ${((3600*h)+(60*m)+s)}`);
    return ((3600*h)+(60*m)+s);
}

var timestampFromSeconds = (duration) => {
    //3894
    var hours = Math.floor(duration/3600);  //1.08==1
    var minutes = Math.floor((duration - (hours*3600))/60);//4.9=4
    var seconds = duration - ((60*minutes)+(3600*hours));//54

    var timeStamp = getTimestamp({
        h: hours,
        m: minutes,
        s: seconds
    });

    return timeStamp;
}

var getTimestamp = (duration) => {
    //"HH:MM:SS"

    //Parses timestamp & formats into 2-digit string
    var hrNumber = duration['h']!=0 ? parseInt(duration['h']) : 0;
    var hrTimestamp = (hrNumber==0) ? `00` : 
        (hrNumber<10 ? `0${hrNumber}` : `${hrNumber}`);

    var minNumber = duration['m']!=0 ? parseInt(duration['m']) : 0;
    var minTimestamp = (minNumber==0) ? `00` : 
        (minNumber<10 ? `0${minNumber}` : `${minNumber}`);

    var secNumber = duration['s']!=0 ? parseInt(duration['s']) : 0;
    var secTimestamp = (secNumber==0) ? `00` : 
        (secNumber<10 ? `0${secNumber}` : `${secNumber}`);

    var finalTimestamp = `${hrTimestamp}:${minTimestamp}:${secTimestamp}`
    console.log(`Duration: ${duration} Timestamp: ${finalTimestamp}`);

    return finalTimestamp;
}

testCut()

//Test 1 Results
/*
 * original: relativePath, entireVidDuration
 * exclude: startTime=0, duration=30s
 * result: 2 clips, 1 starting @ 0:30 - duration 2:19 (fullDur-30)
 *         Other unreadable, 0 start 0 duration
 */

 //Test 2 Results
 /* 
  * original: same as Test1
  * exclude: same as Test1 ++ startTime=60, duration=(fullVid-60)(so the rest)
  * result: desired result. 3 clips, middle clip being the desired clip
  */