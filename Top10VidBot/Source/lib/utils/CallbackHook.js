//Methods for calling an async function for each element in an array &
//calling action on finish OR gathering results

// Call async function for each element of array, run onFinish callback once all complete
var callbackHook = (givenArray, actionFunction, onIterate, onErr, onFinish) => {
    var counter = 0;

    //Iterate thru given array
    for(var element of givenArray){

        //Call given function w element & callback(err, result)
        actionFunction(element, (err, result) => {
            counter++;
            if(!err){
                onIterate(result);
            }
            else{
                if(onErr){
                    onErr(err);
                }
            }

            //Once all async operations are complete, initiate finished callback
            if(counter==givenArray.length){
                if(onFinish){
                    onFinish(givenArray);
                }
            }
        })
    }
}

// Call async function for each element of array, collect results of each operation, return aggregated results on finish
var callbackGather = (givenArray, actionFunction, onMod, onErr, onFinish) => {
    var counter = 0;
    //Array to store valid Results
    var resultCollection = [];

    //Iterate thru given array
    for(var element of givenArray){
        //Call given function w element & callback(err, result)
        actionFunction(element, (err, result) => {
            counter++;
            if(!err){
                if(onIterate){
                    result = onIterate(result);
                }
                resultCollection.push(result);
            }
            else{
                if(onErr){
                    onErr(err);
                }
            }

            if(counter==givenArray.length){
                if(onFinish){
                    onFinish(resultCollection);
                }
            }
        })
    }
}

//Testing

// callbackGather([{t1: 1, t2: 2}, {t1: 3, t2: 4}],
//     (item, callback) => {
//         if(item['t1']==3){
//             callback(null, item);
//         }
//         else{
//             callback(`Not 3`, null);
//         }
//     },
//     (result) => {
//         var nresult = {t1: 99, t2: 99};
//         return nresult;
//     },
//     (err) => {
//         console.log(err)
//     },
//     (gathered) => {
//         console.log(gathered);
//     }
// )

module.exports = {
    callbackHook,
    callbackGather
}