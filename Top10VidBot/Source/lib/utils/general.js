let condenseChars = [
    ' ',
    '/',
    '\'',
    '\\',
    ':'
]

let genreCross = {
    'Real Time Strategy (RTS)': 'Real Time Strategy',
    'Role-playing (RPG)': 'Role playing',
    'Turn-based strategy (TBS)': 'Turn-based strategy',
    'Platform': 'Platformer',
    'Hack and slash/Beat \'em up': 'Beat em up',
    'Quiz/Trivia': 'Trivia',
    'Simulator': 'Simulation'
}

var condenseString = (toCondense) => {
    for(var char of condenseChars){
        while(toCondense.includes(char)){
            toCondense = toCondense.replace(char, '');
        }
    }

    return toCondense;
}

var crosswalkGenre = (genre) => {
    if(genreCross.hasOwnProperty(genre)){
        return genreCross[genre];
    }
    return genre;
}

module.exports = {
    condenseString,
    crosswalkGenre
}