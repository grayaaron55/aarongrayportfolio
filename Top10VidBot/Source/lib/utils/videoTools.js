//  ffmpeg -i [inputFile] [options] [outputFile]
//Volume change: -vol [integer]
let silentMode = true;

//Audio strip: -an
var stripAudio = (videoPath, outputPath, callback) => {

    let commandQuery =
        `ffmpeg -i ${videoPath} -an ${outputPath} -y`;

    let child = shelljs.exec(commandQuery, { async: true, silent: silent });

    child.on('exit', (code, signal) => {

        if (code === 0) {
            callback(null, outputPath);
        }
        else { 
            callback({ err: {code: code, signal: signal}}, null); 
        }

    });
}

var replaceAudio = (videoPath, newAudioPath, outputPath, callback) => {
    let commandQuery = 
        `ffmpeg -i ${videoPath} -i ${newAudioPath} -map 0:0 -map 1:0 ${outputPath} -y`;

    child.on('exit', (code, signal) => {
        
        if (code === 0) {
            callback(null, outputPath);
        }
        else { 
            callback({ err: {code: code, signal: signal}}, null); 
        }

    });
}

//Process:
//  Attach scripted audio to entry footage
//  Combine all body footage
//  Later, Extract audio from whole video, merge w music, reattach

module.exports = {
    replaceAudio
}