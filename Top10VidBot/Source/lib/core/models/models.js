let videoDuration = {
    h: 0,
    m: 0,
    s: 0
}

let videoInfo = {
    game: " ",
    videoId: " ",
    title: " ",
    channelName: " ",
    length: videoDuration
}

class Timestamp
{
    constructor(_hours, _minutes, _seconds){
        this.splitDuration = getSplitDuration(_hours, _minutes, _seconds);
        this.timestampString = getTimestamp(_hours, _minutes, _seconds);
        this.totalSeconds = getTotalSeconds(_hours, _minutes, _seconds);
    }

    getTotalSeconds(_h, _m, _s){
        var h = _h==0 ? 0 : parseInt(_h, 10);
        var m = _m==0 ? 0 : parseInt(_m, 10);
        var s = _s==0 ? 0 : parseInt(_s, 10);

        var totalSeconds = ((3600*_h)+(60*_m)+_s)
        return totalSeconds;
    }

    getSplitDuration(_h, _m, _s){
        var splitDuration = {
            h: _h,
            m: _m,
            s: _s
        };
        
        return splitTimestamp;
    }

    getSplitDuration(_seconds){
        //Floor hr/minute so that seconds can be the remainder
        var hours = Math.floor(totalSeconds/3600);
        var minutes = Math.floor((totalSeconds - (hours*3600))/60);
        var seconds = totalSeconds - ((60*minutes)+(3600*hours));

        var duration = {
            h: hours,
            m: minutes,
            s: seconds
        };

        return duration;
    }

    getTimestamp(h, m, s){
        //Parse hours
        var hrNumber = h!=0 ? parseInt(h) : 0;
        var hrTimestamp = (hrNumber==0) ? `00` : 
            (hrNumber<10 ? `0${hrNumber}` : `${hrNumber}`);

        //Parse minutes
        var minNumber = m!=0 ? parseInt(m) : 0;
        var minTimestamp = (minNumber==0) ? `00` : 
            (minNumber<10 ? `0${minNumber}` : `${minNumber}`);

        //Parse seconds
        var secNumber = s!=0 ? parseInt(s) : 0;
        var secTimestamp = (secNumber==0) ? `00` : 
            (secNumber<10 ? `0${secNumber}` : `${secNumber}`);
        
        //Combine all 2-digit conversions into timestamp
        var finalTimestamp = `${hrTimestamp}:${minTimestamp}:${secTimestamp}`

        return finalTimestamp;
    }

    secondsToDuration(totalSeconds){
    
        //Floor hr/minute so that seconds can be the remainder
        var hours = Math.floor(totalSeconds/3600);
        var minutes = Math.floor((totalSeconds - (hours*3600))/60);
        var seconds = totalSeconds - ((60*minutes)+(3600*hours));
    
        var duration = {
            h: hours,
            m: minutes,
            s: seconds
        };
    
        return duration;
    }
}

module.exports = {
    Timestamp
}