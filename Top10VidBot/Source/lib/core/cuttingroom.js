let cfg = require('./cfg').cfg;
let cutter = require('video-stitch');
let logger = require('./logger');
let canti = require('./Canti');
let vidTools = require('../utils/videoTools');
let models = require('./models/models');
let Timestamp = models.Timestamp;

let wavInfo = require('wav-file-info');
let ffProbe = require('ffprobe');
let ffProbeStatic = require('ffprobe-static');

let episodeVideoPath = `../../media/video/downloaded`;
let episodeAudioPath = `../../media/audio/canti/generated/episodes`;
var absolutePath = `/Users/pixelluminati/desktop/algoclass/kotakubot/media/video/downloaded`;
// let clipsPerEntry = 3;
let clipsPerEntry = cfg['clipsPerEntry'];

// This is where preliminary video arrangement happens

/*
{
    gameData: { 
        keyword: { 
            name: 'anarchism', 
            id: 7335 
        },
        genre: { 
            name: 'Beat em up', 
            id: 25 
        },
        games: [{
            'name': game['name'],
            'id': game['id'],
            'rating': game['rating'],
            'summary': game['summary']
        }] 
    },
    episodeId: '25_7335',
    videoData: [{ 
        videosInfo: [{ 
            videoId: '548wz92w5Lg',
            title: 'Multiplayer Gameplay Trailer | Assassin\'s Creed 4 Black Flag [UK]',
            timestamp: Timestamp object,
            channelName: 'Ubisoft' 
        }],
        gameName: 'TheLegendofZelda:ALinkBetweenWorlds',
        episodeId: '31_301' 
    }],
    scriptData: {
        intro: '',
        outtro: '',
        entries: [{
            script: '',
            game: '',
            rank: 0
        }]
    }
}
*/
var cutFootage = (videoInfo, callback) => {
    logger.info("Moving into Cutting stage")

    var footagePath = `${episodeVideoPath}/${videoInfo['episodeId']}`;
    absolutePath +=`/${videoInfo['episodeId']}`;

    for(var game of videoInfo['videoData']){
        var videoPaths = [];
        for(var video of game['videoData']){
            var path = `${footagePath}/${game['game']}/${game['game']}_${video['videoId']}.mp4`;
            var finalPath = absolutePath+`/${game['game']}/${game['game']}_${video['videoId']}.mp4`;
            videoPaths.push({
                path: path,
                absolutePath: finalPath,
                duration: video['timestamp']
            });
        }

        var mergedOutputs = [];

        for(var entry of videoInfo['scriptData']['entries']){            
            if(entry['game']==game['game']){
                console.log(`Cropping rank ${entry['rank']}: ${game['game']}`)
                game['epId'] = videoInfo['episodeId'];
                game['rank'] = entry['rank'];
                
                cropClips(videoInfo['episodeId'], videoPaths, entry['rank'], game, (err, clipsInfo, gameInfo) => {
                    if(err){
                        logger.err(err);
                        return;
                    }
                    var outputPath = absolutePath+`/${gameInfo['game']}/${gameInfo['game']}_RAWSPLICED.mp4`;
                    var audioPath = cfg['episodeAudioPath']+`/${gameInfo['epId']}/entry_${gameInfo['rank']}.wav`;
                    var output = {
                        raw: outputPath,
                        voiced: absolutePath+`/entry_${gameInfo['rank']}.mp4`
                    }

                    logger.info(`Splicing clips for ${gameInfo['game']}`)
                    spliceClips(clipsInfo, output, audioPath, (err, splicedVid) => {
                        console.log(`Splice complete: ${splicedVid}`)
                        mergedOutputs.push({
                            fileName: splicedVid
                        })

                        if(mergedOutputs.length==videoInfo['scriptData']['entries'].length){
                            var mergedBodyPath = absolutePath + `/bodyFootage.mp4`;
                            spliceClips(mergedOutputs, mergedBodyPath, (err, splicedVideoBody) => {
                                console.log(`O M E G A S P L I C E  C O M P L E T E`);
                                console.log(splicedVideoBody);

                                //Also need the Intro, Outtro, CallToAction, & HonorableMentions cut
                                callback(null, splicedVideoBody);
                            })
                        }
                    })
                })
            }
        }
    }
}

var cropClips = (epId, vidPaths, rank, game, callback) => {
    logger.info(`Cutting entry ${rank}`);
    var path = `${episodeAudioPath}/${epId}/entry_${rank}.wav`;

    wavInfo.infoByFilename(path, (err, info) => {
        var clipDuration = Math.round(info['duration']);
        var slotDuration = Math.round(clipDuration/vidPaths.length);

        var totalDur = 0;
        var clippingInfo = [];
        for(var vid of vidPaths){
            totalDur += slotDuration;
            var clipDur = slotDuration;
            //on final pass
            if(clippingInfo==vidPaths.length-1){
                if(totalDur<clipDuration){
                    //Add the diff between expected length & estimated
                    clipDur += Math.round(clipDuration-(slotDuration*vidPaths.length));
                }
            }

            clippingInfo.push({
                path: vid,
                clippedDuration: clipDur
            })

            //After last addition
            if(clippingInfo.length==vidPaths.length){
                logger.info('Extracting clips...')
                var finalCuts = [];

                for(var clip of clippingInfo){

                    extractClip(clip, rank, (err, clipInfo) => {
                        if(err){
                            console.log(err);
                            logger.err(`Error after extract`)
                            callback(`Error on extraction`, null, null);
                            return;
                        }
                        logger.info(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`)
                        console.log(clipInfo)
                        logger.info(`~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`) 
                        
                        finalCuts.push(clipInfo);

                        //After all collected
                        if(finalCuts.length==clippingInfo.length){
                            // console.log('finalcuts:'+finalCuts)
                            callback(null, finalCuts, game);
                        }
                    });
                }
            }
        }
    })
}

var extractClip = (vidInfo, rank, callback) => {
    let defaultStartPoint = 3;
    
    var durationInfo = vidInfo['path']['duration'];
    
    var clipDurationTimestamp = secondsToTimestamp(vidInfo['clippedDuration']);
    var vidDurationInSeconds = durationToSeconds(durationInfo);

    //Determine start point for cut
    var startTimeSeconds = 0;
    var startTimestamp = secondsToTimestamp(0);

    //If video is big enough, start at a point in the video. Otherwise, just start at start
    if(vidDurationInSeconds/2>vidInfo['clippedDuration']){
        startTimeSeconds = Math.floor(vidDurationInSeconds/defaultStartPoint);
        startTimestamp = secondsToTimestamp(startTimeSeconds);
    }
    // console.log(`${getTimestamp(durationInfo)} vs ${timestampFromSeconds(vidDurationInSeconds)}`);

    var options = {
        silent: true
    }
    cutter.cut(options)
        .original({
            "fileName": vidInfo['path']['path'],
            "duration": getTimestamp(durationInfo)
        })
        .exclude([
        //Pre-clipped
        {
            "startTime": secondsToTimestamp(0),
            "duration": secondsToTimestamp(startTimeSeconds)
        },
        //Post-clipped
        {
            "startTime": secondsToTimestamp(startTimeSeconds+vidInfo['clippedDuration']),
            "duration": secondsToTimestamp(vidDurationInSeconds-(startTimeSeconds+vidInfo['clippedDuration']))
        }])
        .cut()
        .then(
            //Middle clip SHOULD be the one needed. Check its duration against expected
            (videoClips) => {
                logger.info(`Clipping complete for ${vidInfo['path']['path']}`)
                for(var clip of videoClips){
                    console.log(clip['duration']);
                    console.log(secondsToTimestamp((startTimeSeconds+vidInfo['clippedDuration'])-startTimeSeconds));
                    if(clip['duration']==secondsToTimestamp((startTimeSeconds+vidInfo['clippedDuration'])-startTimeSeconds)){
                        callback(null, {clip: clip, rank: rank })
                        return;
                    }
                }
                callback(`Cropped clip not found for ${vidInfo['path']['path']}; clipDuration==${clip['duration']}`, null);
                return;
        })
        .catch(err => {
            logger.err(`Error cutting clip ${vidInfo['path']['path']}`)
            callback(err, null);
            return;
        })
}

var spliceClips = (clips, output, audioPath, callback) => {

    //Get file name of each clip for splice
    var fileNames = [];
    for(var clip of clips){
        //Data is formatted in a way to deliver game rank separate from clip info
        var realClip = clip['clip'];
        fileNames.push({
            fileName: realClip['fileName']
        });
    }

    //Silent determines whether ffmpeg outputs to console
    var options = {
        silent: false
    }

    //Function to perform the splice
    cutter
    .concat(options)
    .clips(fileNames)
    .output(output['outputPath'])
    .concat()
    .then(
        (outputFileName) => {

            logger.info('Splice complete. Output at: '+outputFileName);

            //Replace spliced footage with the narrative audio overlay
            vidTools.replaceAudio(outputFileName, audioPath, output['voiced'], 
                (err, filePath) => {
                    if(err){
                        logger.err(`Error adding audio to spliced clip`)
                        callback(err, null);
                        return;
                    }
                    //Return the filepath of the spliced footage
                    callback(null, filePath);
                    return;
                }
            );
        })
    .catch(
        (err) => {
            logger.err(`Error on splicing clips.`);
            console.log(fileNames);
            callback(err, null);
            return;
        }
    )
}

//Parses Duration object { h: 0, m: 0, s: 0} into HH:MM:SS timestamp string
var getTimestamp = (duration) => {

    //Parse hours
    var hrNumber = duration['h']!=0 ? parseInt(duration['h']) : 0;
    var hrTimestamp = (hrNumber==0) ? `00` : 
        (hrNumber<10 ? `0${hrNumber}` : `${hrNumber}`);

    //Parse minutes
    var minNumber = duration['m']!=0 ? parseInt(duration['m']) : 0;
    var minTimestamp = (minNumber==0) ? `00` : 
        (minNumber<10 ? `0${minNumber}` : `${minNumber}`);

    //Parse seconds
    var secNumber = duration['s']!=0 ? parseInt(duration['s']) : 0;
    var secTimestamp = (secNumber==0) ? `00` : 
        (secNumber<10 ? `0${secNumber}` : `${secNumber}`);
    
    //Combine all 2-digit conversions into timestamp
    var finalTimestamp = `${hrTimestamp}:${minTimestamp}:${secTimestamp}`

    return finalTimestamp;
}

//Convert a Duration object { h: 0, m: 0, s: 0 } to total time in seconds
var durationToSeconds = (info) => {
    var h = info['h']==0 ? 0 : parseInt(info['h'], 10);
    var m = info['m']==0 ? 0 : parseInt(info['m'], 10);
    var s = info['s']==0 ? 0 : parseInt(info['s'], 10);

    var totalSeconds = ((3600*h)+(60*m)+s)
    return totalSeconds;
}

//Generate a Duration object { h: 0, m: 0, s: 0 } from a # of seconds
var secondsToDuration = (totalSeconds) => {
    
    //Floor hr/minute so that seconds can be the remainder
    var hours = Math.floor(totalSeconds/3600);
    var minutes = Math.floor((totalSeconds - (hours*3600))/60);
    var seconds = totalSeconds - ((60*minutes)+(3600*hours));

    var duration = {
        h: hours,
        m: minutes,
        s: seconds
    };

    return duration;
}

//Convert an amount of seconds into a Timestamp HH:MM:SS
var secondsToTimestamp = (totalSeconds) => {
    //Convert seconds to duration obj
    var durationObj = secondsToDuration(totalSeconds);

    //Convert duration obj to timestamp
    var timeStamp = getTimestamp(durationObj);

    return timeStamp;
}

module.exports = {
    cutFootage
}