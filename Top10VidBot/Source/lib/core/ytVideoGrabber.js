let fs = require('fs');
let download = require('ytdl-core');
let Youtube = require('youtube-node');
let shuffle = require('shuffle-array');
let logger = require('./logger');
let cfg = require('./cfg').cfg;
let gather = require('../utils/callbackhook').callbackGather;
let hook = require('../utils/callbackhook').callbackHook;

let youTube = new Youtube();
youTube.setKey(ytAPIKey);

//I dont even think this is getting used? Think i was trying to rewrite the YT functionality w/ callback hook
var getFootage = (gameData, episodeId, callback) => {
    
    gather(gameData, 
        //Action
        (item, callback) => {
            var gameName = item['name'];
            var searchQuery = `${gameName} ${cfg['extraTags']}`;
            
            youTube.search(searchQuery, cfg['maxQueryResults'], (err, results) => {
                if(err){
                    callback(err, null);
                }

                if(results['items'].length<=0){
                    callback(`No results found for \"${gameName}\"`, null);
                }

                //Gather results of yt GetById
                gather(results['items'],
                (item, vidInfoCallback) => {
                    youTube.getById(item['id']['videoId'], (err, result) => {
                        if(result&&result.length>0){
                            var vidItem = result['items'][0];
                            //Get time data from duration string
                            var duration = vidItem['contentDetails']['duration'];
                            var hours = duration.includes('H') ? duration.slice(duration.search(/([0-9][0-9]H)|([0-9]H)/g), duration.indexOf('H')) : 0;
                            var minutes = duration.includes('M') ? duration.slice(duration.search(/([0-9][0-9]M)|([0-9]M)/g), duration.indexOf('M')) : 0;
                            var seconds = duration.includes('S') ? duration.slice(duration.search(/([0-9][0-9]S)|([0-9]S)/g), duration.indexOf('S')) : 0;
                            
                            

                            if(minutes<cfg['maxVideoMinutes'] && minutes>=cfg['minVideoMinutes'] && hours==0 && !cfg['channelBlacklist'].includes(vidItem['snippet']['channelTitle'])){
                                vidInfoCallback(null, vidItem)
                            }
                        }
                        else{
                            vidInfoCallback(`No results for ${item['id']}`, null);
                        }
                    })
                },
                (modItem) => {
                    var duration = modItem['contentDetails']['duration'];
                    var hours = duration.includes('H') ? duration.slice(duration.search(/([0-9][0-9]H)|([0-9]H)/g), duration.indexOf('H')) : 0;
                    var minutes = duration.includes('M') ? duration.slice(duration.search(/([0-9][0-9]M)|([0-9]M)/g), duration.indexOf('M')) : 0;
                    var seconds = duration.includes('S') ? duration.slice(duration.search(/([0-9][0-9]S)|([0-9]S)/g), duration.indexOf('S')) : 0;
                    var videoLength = {
                        's': seconds,
                        'm': minutes,
                        'h': hours
                    }

                    var data = {
                        game: gameName,
                        videoId: modItem['id'],
                        title: modItem['snippet']['title'],
                        length: videoLength,
                        channelName: modItem['snippet']['channelTitle']
                    }

                    return data;
                },
                (err) => {
                    callback(err, null);
                },
                (collected) => {
                    logger.info(`${collected.length} results returned for ${gameName}`);
                    callback(null, collected);
                })
            })
        },
        //Modifier function for successful entry
        (modItem) => {
            var gameName = modItem['game'];

            var vidInfo = {
                videosInfo: modItem.slice(0,cfg['clipsPerGame']),
                game: gameName,
                episodeId: episodeId 
            }

            //ensure path exists
            var downloadPath = cfg['episodeVideoPath'];
            if(!fs.existsSync(`${downloadPath}/${episodeId}/${info['game']}`)){
                if(!fs.existsSync(`${downloadPath}/${episodeId}`)){
                    logger.info(`Creating directory: ${downloadPath}/${episodeId}`);
                    fs.mkdirSync(`${downloadPath}/${episodeId}`);
                }
                logger.info(`Creating directory: ${downloadPath}/${episodeId}/${info['game']}`);
                fs.mkdirSync(`${downloadPath}/${episodeId}/${info['game']}`);
            }

            var ctr = 0;
            for(var video of vidInfo['videosInfo']){
                //Enable action after download complete
                var stream = fs.createWriteStream(`${downloadPath}/${episodeId}/${vidInfo['game']}/${vidInfo['game']}_${video['videoId']}.mp4`);
                
                stream.on('close', () => {
                    ctr++;
                    //Finished with all
                    if(ctr==info['videosInfo'].length){
                        logger.info(`${info['videosInfo'].length} videos downloaded for ${info['game']}...`);
                        callback(null, info);
                        return;
                    }
                });

                // download(/)
                //     .pipe(stream);
            }
        }
)
}
