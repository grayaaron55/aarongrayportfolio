let tracery = require('tracery-grammar');
let sortBy = require('sort-array');
let logger = require('./logger');
let recordingBooth = require('./recordingbooth');
let cfg = require('./cfg').cfg;

let numberMentions = cfg['numberMentions'];

var gamesData;

// Use tracery & episode clip data to generate a script
var generateScript = (data, episodeId, callback) => {
    gamesData = data['games'];

    //This sorts the games in order of their given Rating. Fuzzing would go here
    sortBy(gamesData, 'rating');

    var introScript = getIntro(data);
    var outtroScript = getOuttro(data);

    // Create objects for intro/outtro & pass to TTS generator
    var introRecord = {
        type: 'intro',
        script: introScript,
        episodeId: episodeId,
        rank: null
    }
    var outtroRecord = {
        type: 'outtro',
        script: outtroScript,
        episodeId: episodeId,
        rank: null
    }

    recordingBooth.recordLine(introRecord, (err) => {
        recordingBooth.recordLine(outtroRecord, (err) => {
            var ctr = 0;
            var entryScripts = [];

            // Get entry scripts for ranked games
            for (var i = 3; i < gamesData.length; i++) {
                var entryText = getEntryScript(gamesData[i], (gamesData.length - i));
                var condensedName = condenseName(gamesData[i]['name']);

                entryScripts.push({
                    script: entryText,
                    game: condensedName,
                    rank: (gamesData.length - i)
                });

                var toRecord = {
                    type: 'entry',
                    script: entryText,
                    episodeId: episodeId,
                    rank: (gamesData.length - i)
                }

                // console.log(toRecord);

                recordingBooth.recordLine(toRecord, (err) => {
                    if (err) {
                        callback(err, null);
                        return;
                    }
                    // logger.info(`${gamesData[i]['name']} entry recorded.`);
                    ctr++;

                    if (ctr == gamesData.length - 3) {
                        var tstOuttro = '';
                        var fullScript = {
                            intro: introScript,
                            outtro: tstOuttro,
                            entries: entryScripts
                        }
                        callback(null, fullScript)
                    }
                })

            }
        })
    })
}

//Generate canned intro from episode info
var getIntro = (data) => {
    var keyword = data['keyword']['name'];
    var genre = data['genre']['name'];
    var gameCount = data['games'].length;

    // TODO: Optimize out the "Today we'll look at", add variance
    var introScript = {
        'origin': `#greeting# Welcome to CantiBot, and today we\'re counting down the Top ${data['games'].length - numberMentions} ${keyword} ${genre} games. #GEN_${data['genre']['name']}#`,
        'greeting': ['Welcome!', 'Welcome back!', 'Greetings!', 'Howdy!', 'Howdy there!', 'Hello and welcome!'],
        'GEN_Fighting': `Today we\'ll be taking a look at ${keyword} fighting games`,
        'GEN_Point-and-click': `Today we\'ll look at ${keyword} point and click games`,
        'GEN_Shooter': `Today we\'ll look at ${keyword} shooter games`,
        'GEN_Music': `Today we\'ll look at ${keyword} music games`,
        'GEN_Platform': `Today we\'ll look at ${keyword} platform games`,
        'GEN_Puzzle': `Today we\'ll look at ${keyword} puzzle games`,
        'GEN_Racing': `Today we\'ll look at ${keyword} racing games`,
        'GEN_Real Time Strategy (RTS)': `Today we\'ll look at ${keyword} real time strategy games`,
        'GEN_Role-playing (RPG)': `Today we\'ll look at ${keyword} role playing games`,
        'GEN_Simulator': `Today we\'ll look at ${keyword} simulator games`,
        'GEN_Sport': `Today we\'ll look at ${keyword} sports games`,
        'GEN_Strategy': `Today we\'ll look at ${keyword} strategy games`,
        'GEN_Turn-based strategy (TBS)': `Today we\'ll look at ${keyword} turn based strategy games`,
        'GEN_Tactical': `Today we\'ll look at ${keyword} tactical games`,
        'GEN_Hack and slash/Beat \'em up': `Today we\'ll look at ${keyword} hack and slash games`,
        'GEN_Quiz/Trivia': `Today we\'ll look at ${keyword} trivia games`,
        'GEN_Pinball': `Today we\'ll look at ${keyword} pinball games`,
        'GEN_Adventure': `Today we\'ll look at ${keyword} adventure games`,
        'GEN_Indie': `Today we\'ll look at ${keyword} indie games`,
        'GEN_Arcade': `Today we\'ll look at ${keyword} arcade games`
    }

    var introGrammar = tracery.createGrammar(introScript);
    return introGrammar.flatten('#origin#');
}

var getOuttro = (data) => {
    var keyword = data['keyword']['name'];
    var genre = data['genre']['name'];
    var gameCount = data['games'].length;

    var outtroScript = {
        'origin': 'Your list is complete. Thank you for watching. Subscribe to CantiBot. Love CantiBot.'
    }

    var outtroGrammar = tracery.createGrammar(outtroScript);
    return outtroGrammar.flatten('#origin#');
}

//Honorable mentions has no dialogue besides canned intro line

//Generate Entry script (including Honorable Mentions)
//RANDOMIZE ORIGIN FOR GRAMMARS
var getEntryScript = (game, rank) => {
    logger.info('Creating entry script for ' + game['name']);
    var score = Math.floor(game['rating']);

    var entryScript = {
        'origin': `Number ${rank}. ${game['name']}.. #flavorIntro# ${game['summary']}. #flavorConclusion#`,
        'flavorIntro': `#INTRO_ratingStatement# ${game['name']} #INTRO_receptionStatement#`,
        'INTRO_ratingStatement': [
            `With an average score of ${score},`,
            `Receiving a critical score of ${score},`,
            `Regardless of how it did on the market,`,
            `Gaining much critical acclaim,`,
            `Attaining the critical score of ${score} overall,`,
            `Generally given a score of around ${score},`,
            `Despite receiving an average review score of ${score},`,
            `Even though #INTRO_enemies# averaged giving it a score of ${score},`,
            `Although it only received a score of ${score} at launch,`,
            `After much #INTRO_afterMuch# and #INTRO_afterMuch#,`
        ],
        'INTRO_afterMuch': [
            'delay',
            'hype',
            'heartache',
            'pain',
            'torment',
            'suffering',
            'crying',
            'fighting',
            'endurance',
            'power',
            'drinking',
            'smoking'
        ],
        'INTRO_enemies': [
            `journalists`,
            `journos`,
            `morons`,
            `fools`,
            `${gamesData[Math.floor(Math.random() * gamesData.length)]['name']} fans`,
            `${gamesData[Math.floor(Math.random() * gamesData.length)]['name']} developers`,
            `${game['name']} developers`
        ],
        'INTRO_receptionStatement': [
            `was a #INTRO_gameStateAdj# #INTRO_gameState#.`,
            `#INTRO_gameVerbed# video games for good.`,
            `ushered in a new age of play.`,
            `turned out to be pretty #INTRO_gameStateAdj#.`,
            `is still worthy of #INTRO_reverence# today.`,
            `developers #INTRO_gameVerbed# at least ${Math.floor(Math.random() * 90)} people in the year 200${Math.floor(Math.random() * 9)}.`,
        ],
        'INTRO_reverence': [
            'pity',
            'praise',
            'awe',
            'reverence',
            'playing'
        ],
        'INTRO_gameVerbed': [
            `ruined`,
            `saved`,
            `destroyed`,
            `improved`,
            `raised`,
            `blew away`,
            `slashed away`,
            `burned`
        ],
        'INTRO_gameState': [
            `success`,
            `tragedy`,
            `epic`,
            `masterpiece`,
            `monster`,
            `disaster`,
            `video game`,
            `production`,
            `spectacle`,
            `experience`,
            `heart attack`,
            `power bomb`,
            `powerhouse`,
            `landmark`,
            `benchmark`
        ],
        'INTRO_gameStateAdj': [
            `decent`,
            `alright`,
            `blasphemous`,
            `sacriligious`,
            `beautiful`,
            `disgusting`,
            `dark`,
            `fantastic`,
            `twisted`,
            `bizarre`,
            `interesting`
        ],
        'flavorConclusion': `#CONC_gameCreditDue# ${game['name']} was #CONC_bridge# #CONC_gameState#`,
        'CONC_gameCreditDue': [
            `With all of this in mind,`,
            `That being said,`,
            `Knowing a little more about the game,`,
            `In regards to all of this,`,
            `With a score of ${score} on the table,`,
            `Thinking back,`,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``,
            ``
        ],
        'CONC_bridge': [
            `truly`,
            `honestly`,
            `barely`,
            `sadly`,
            `thankfully`,
            `unfortunately`,
            `remorsefully`,
            `unbelievably`,
            `confusingly`,
            `doggedly`,
            `solidly`,
            `completely`,
            `surprisingly`
        ],
        'CONC_gameState': [
            `a success`,
            `a tragedy`,
            `an epic`,
            `a masterpiece`,
            `a monster`,
            `a disaster`,
            `a video game, alright`,
            `a game, alright`,
            `a production`,
            `a spectacle`,
            `an experience`
        ]
    }

    var entryScriptGrammar = tracery.createGrammar(entryScript);
    return entryScriptGrammar.flatten('#origin#');
}

var condenseName = (gameName) => {
    var oldName = gameName;
    while (gameName.includes(' ')) {
        gameName = gameName.replace(' ', '')
    }
    while (gameName.includes(':')) {
        gameName = gameName.replace(':', '');
    }
    while (gameName.includes('/')) {
        gameName = gameName.replace('/', '');
    }

    // logger.info(`${oldName} condensed to ${gameName}`);

    return gameName;
}

//Outro will be some canned Call to Action consistent across all vids

module.exports = {
    generateScript
}