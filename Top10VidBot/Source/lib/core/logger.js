let fs = require('fs')

let writeEvery = 20;

var logCtr = 0;

var info = (msg) => {
    console.log("~info~: "+msg)
    
    Update()
}

var err = (msg) => {
    console.log("~ERR~: "+msg)
    
    Update()
}

var Update = () => {
    logCtr++

    if(logCtr>=writeEvery){
        logCtr = 0
        //Save/Update log
    }
}

module.exports = {
    info,
    err
}