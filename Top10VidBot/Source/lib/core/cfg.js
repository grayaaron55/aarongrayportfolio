let cfg = {
    igdbAPIkey: '',
    ytAPIKey: '',
    maxVideoMinutes: 5,
    minVideoMinutes: 1,
    minQueryResults: 5,
    maxQueryResults: 30,
    minMentions: 2,
    maxMentions: 3,
    minEntries: 3,
    maxEntries: 8,
    numberMentions: 2,  
    clipsPerEntry: 3,    
    clipsPerGame: 3,
    extraTags: 'video game gameplay',
    channelBlacklist: [
        'GameGrumps',
        // 'GameSpot',
        // 'IGN',
        'Kotaku'
    ],
    episodeVideoPath: `../../media/video/downloaded`,
    episodeAudioPath: `../../media/audio/canti/generated/episodes`,
}

module.exports = {
    cfg
}