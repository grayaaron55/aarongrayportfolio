let canti = require('./Canti');
let logger = require('./logger')

let fs = require('fs');

var episodeAudioPath = `../../media/audio/canti/generated/episodes`;

var recordLine = (lineInfo, callback) => {
    
    switch(lineInfo['type']){
        case 'intro':
            recordIntro(lineInfo, callback);
            break;
        case 'outtro':
            recordOuttro(lineInfo, callback);
            break;
        case 'entry':
            recordEntry(lineInfo, callback);
            break;
        default:
            break;
    }
}

var recordIntro = (lineInfo, callback) => {
    if(!fs.existsSync(`${episodeAudioPath}/${lineInfo['episodeId']}`)){
        logger.info(`Creating directory: ${episodeAudioPath}/${lineInfo['episodeId']}`);
        fs.mkdirSync(`${episodeAudioPath}/${lineInfo['episodeId']}`);
    }


    canti.record(lineInfo['script'], `${episodeAudioPath}/${lineInfo['episodeId']}/intro.wav`, (err) => {
        if(err){
            callback(err);
            return;
        }

        callback(null);
    })
}

var recordOuttro = (lineInfo, callback) => {
    if(!fs.existsSync(`${episodeAudioPath}/${lineInfo['episodeId']}`)){
        logger.info(`Creating directory: ${episodeAudioPath}/${lineInfo['episodeId']}`);
        fs.mkdirSync(`${episodeAudioPath}/${lineInfo['episodeId']}`);
    }

    canti.record(lineInfo['script'], `${episodeAudioPath}/${lineInfo['episodeId']}/outtro.wav`, (err) => {
        if(err){
            callback(err);
            return;
        }

        callback(null);
    })
}

var recordEntry = (lineInfo, callback) => {
    if(!fs.existsSync(`${episodeAudioPath}/${lineInfo['episodeId']}`)){
        logger.info(`Creating directory: ${episodeAudioPath}/${lineInfo['episodeId']}`);
        fs.mkdirSync(`${episodeAudioPath}/${lineInfo['episodeId']}`);
    }

    canti.record(lineInfo['script'], `${episodeAudioPath}/${lineInfo['episodeId']}/entry_${lineInfo['rank']}.wav`, (err) => {
        if(err){
            callback(err);
            return;
        }

        callback(null);
    })
}

module.exports = {
    recordLine
}