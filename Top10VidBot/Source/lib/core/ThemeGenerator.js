let igdb = require('igdb-api-node').default;
let capitalize = require('capitalize')
let cfg = require('./cfg').cfg;
let logger = require('./logger');

let igdbClient = igdb(cfg['igdbAPIkey']);

// Roll a theme (video name)
var rollName = (callback) => {
    //10804 Total Keywords
    var rndmIndex = Math.round((Math.random() * 10000));

    igdbClient.keywords({
        fields: '*',
        limit: 1, // Limit to 1 result
        offset: rndmIndex // Index offset for results
    })
    .then(
        (response) => {
            if (response['body'].length != 1) {
                callback(`No keyword found at offset ${rndmIndex}`, null);//error of not found
                return;
            }

            var chosenKeyword = response['body'][0]['name'];
            var keywordGameIds = response['body'][0]['games'];
            var keywordId = response['body'][0]['id'];

            var genreRand = Math.floor((Math.random() * 19));
            igdbClient.genres({
                fields: '*',
                limit: 1,
                offset: genreRand
            })
            .then(
                (response) => {
                    if (response['body'].length != 1) {
                        callback(`No genre found at offset ${genreRand}`, null);//error of not found
                        return;
                    }

                    var chosenGenre = response['body'][0]['name'];
                    var genreGameIds = response['body'][0]['games'];
                    var genreId = response['body'][0]['id'];

                    logger.info(`Testing Combo: ${chosenKeyword} : ${chosenGenre}`)

                    if (chosenGenre == chosenKeyword) {
                        logger.err('Genre equivalent to keyword. Reroll;')
                        rollName(callback);
                        return;
                    }
                    if (genreGameIds && keywordGameIds) {
                        // logger.info(`Processing [ ${chosenKeyword} : ${chosenGenre} ]`)
                        // logger.info('All IDs valid. Intersecting')

                        //Get overlap from 2 gameid arrays & pass to call for games if>0
                        var intersect = genreGameIds.filter(id => keywordGameIds.includes(id));

                        var minGames = cfg['numberMentions'] + cfg['minEntries'];

                        if (intersect.length < minGames) {
                            logger.err(`Not enough results for ${chosenKeyword} : ${chosenGenre}. Reroll;`);
                            rollName(callback);
                            return;
                        }
                        else {
                            var maxGames = cfg['maxEntries'] + cfg['numberMentions'];

                            if(intersect.length > maxGames){
                                intersect = intersect.slice(0, maxGames);
                            }

                            logger.info(`Pulling ${intersect.length} games for ${chosenKeyword} : ${chosenGenre}`);
                            igdbClient.games({
                                ids: intersect,
                                fields: '*'
                            })
                            .then(
                            //Handle info from all intersected Games (members of both keyword&genre)
                            (resp) => {
                                var gameData = [];
                                for (var game of resp['body']) {
                                    //If the game has a rating & a summary & the summary isnt ''
                                    if (game['rating'] && game['summary'] && game['summary']!=''){
                                        //Add Game Data to gameData array
                                        gameData.push({
                                            'name': game['name'],
                                            'id': game['id'],
                                            'rating': game['rating'],
                                            'summary': game['summary']
                                        })
                                    }
                                    else {
                                        // logger.info(`Skipping ${game['name']}, no rating found.`)
                                    }
                                }

                                if (gameData.length < minGames) {
                                    logger.info(`Not enough ratings: [ ${chosenKeyword} : ${chosenGenre} ] --Reroll;`)
                                    rollName(callback);
                                    return;
                                }
                                else {
                                    var returned = {
                                        'keyword': { 'name': chosenKeyword, 'id': keywordId },
                                        'genre': { 'name': chosenGenre, 'id': genreId },
                                        'games': gameData
                                    }
                                    console.log(`~~~~~~~~~~`);
                                    console.log(returned);
                                    console.log(`~~~~~~~~~~`)

                                    callback(null, returned);
                                    return;
                                }
                            })
                        }
                    }
                    else{
                        logger.err('Invalid ID set. Reroll;');
                        rollName(callback);
                        return;
                    }
                })
                .catch(err => {
                    callback(err, null);
                    return;
                });
        })
        .catch(err => {
            callback(err, null);
            return;
        });
}

//Take Game Data from IGDB data & get relevant info
var processGameData = (data, callback) => {


    var gameData = [];
    for (var game of data['body']) {
        //If the game has a rating & a summary & the summary isnt ''
        if (game['rating'] && game['summary'] && game['summary']!=''){
            //Add Game Data to gameData array
            gameData.push({
                'name': game['name'],
                'id': game['id'],
                'rating': game['rating'],
                'summary': game['summary']
            })
        }
        else {
            // logger.info(`Skipping ${game['name']}, no rating found.`)
        }
    }

    if (gameData.length < minGames) {
        logger.info(`Not enough ratings: [ ${chosenKeyword} : ${chosenGenre} ] --Reroll;`)
        rollName(callback);
        return;
    }
    else {
        var returned = {
            'keyword': { 'name': chosenKeyword, 'id': keywordId },
            'genre': { 'name': chosenGenre, 'id': genreId },
            'games': gameData
        }
        console.log(`~~~~~~~~~~`);
        console.log(returned);
        console.log(`~~~~~~~~~~`)

        callback(null, returned);
        return;
    }
}

module.exports = {
    rollName
}