let fs = require('fs');
let download = require('ytdl-core');
let Youtube = require('youtube-node');
let logger = require('./logger');
let cfg = require('./cfg').cfg;
let utils = require('../utils/general');
let Timestamp = require('./models/models').Timestamp;

let youTube = new Youtube();

let ytAPIKey = cfg['ytAPIKey'];
let maxVideoMinutes = cfg['maxVideoMinutes'];
let minVideoMinutes = cfg['minVideoMinutes'];
let maxVideosReturned = cfg['maxQueryResults'];
let clipsPerGame = cfg['clipsPerGame'];
let extraTags = cfg['extraTags'];
let channelBlacklist = cfg['channelBlacklist'];

let downloadPath = cfg['episodeVideoPath'];

youTube.setKey(ytAPIKey);

/*GameData:
games: [{
    name: "Game Name",
    'id': "Game Id",
    'rating': 0,
    'summary': "Summary"
}] 
*/
//Gathers video info from search and calls to download selected videos
var getFootage = (gameData, episodeId, callback) => {
    var ctr = 0;
    var allInfo = [];
    /* 
        allInfo[{
            game: "Game Name",
            episodeId: "55555555_5555",
            videosInfo: [{
                game: "Game Name",
                videoId: "XXXXXX",
                title: "Example Video Title", 
                length: {
                    h: 0,
                    m: 0,
                    s: 0
                }, 
                channelName: "Example Channel 99"
            }]

        }]
    */
    for (var game of gameData) {
        //Search for videos using Game Name
        findVideos(game['name'],
            (err, result) => {
                //If there's a result? I need to fix this
                if (result['videoData'] && result['videoData'].length > 0) {
                    var gameName = result['gameName'];
                    var videoData = result['videoData']

                    //Take first 0-3 results of video search
                    var nonMentionGameVideos = videoData.slice(0, cfg['numberMentions']);

                    gameName = utils.condenseString(gameName);

                    var clipsInfo = {
                        videoData: videoData.slice(0, clipsPerGame),
                        game: gameName,
                        episodeId: episodeId
                    }
                    ctr++;

                    downloadVideos(clipsInfo, (err, vidInfo) => {
                        if (err) {
                            callback(err, null);
                            return;
                        }

                        // ctr++;
                        allInfo.push(vidInfo);

                        if (ctr == gameData.length) {
                            callback(null, allInfo);
                            return;
                        }
                    });
                }
            }
        );
    }
}

var findVideos = (gameName, callback, broad = false) => {
    logger.info(`Performing search: ${gameName} ${broad ? '' : extraTags}`)

    youTube.search(`${gameName} ${broad ? '' : extraTags}`, maxVideosReturned,
        (err, results) => {
            if (err) {
                callback(err, null);
                return;
            }
            else if (results['items'].length <= 0) {
                callback(`No results found for \"${gameName}\"`, null);
                return;
            }
            else if (results['items'].length < cfg['minQueryResults']) {
                callback(`Less than ${cfg['minQueryResults']} for \"${gameName}\"`, null);
                return;
            }



            var videoData = [];

            for (var res of results['items']) {
                var ctr = 0;

                //Get video details for each result
                youTube.getById(res['id']['videoId'], (err, res) => {

                    if (res && res['items'].length > 0) {
                        var item = res['items'][0];

                        //Get time data from duration string
                        var durationString = item['contentDetails']['duration'];
                        var hours = durationString.includes('H') ? durationString.slice(durationString.search(/([0-9][0-9]H)|([0-9]H)/g), durationString.indexOf('H')) : 0;
                        var minutes = durationString.includes('M') ? durationString.slice(durationString.search(/([0-9][0-9]M)|([0-9]M)/g), durationString.indexOf('M')) : 0;
                        var seconds = durationString.includes('S') ? durationString.slice(durationString.search(/([0-9][0-9]S)|([0-9]S)/g), durationString.indexOf('S')) : 0;

                        // var videoTimestamp = new Timestamp(hours, minutes, seconds);
                        var videoTimestamp = {
                            h: hours,
                            m: minutes,
                            s: seconds
                        }

                        //If duration good & not blacklisted, take vid
                        if (minutes < maxVideoMinutes && minutes >= minVideoMinutes && hours == 0 && !channelBlacklist.includes(item['snippet']['channelTitle'])) {
                            videoData.push({
                                videoId: item['id'],
                                title: item['snippet']['title'],
                                timestamp: videoTimestamp,
                                channelName: item['snippet']['channelTitle']
                            });
                        }
                    }

                    ctr++;

                    //On last item...
                    if (ctr == results['items'].length) {
                        logger.info(`All video data (${videoData.length} entries) collected for: ${gameName}`)

                        var allData = {
                            gameName: gameName,
                            videoData: videoData
                        }

                        callback(null, allData);
                        return;
                    }
                });
            }
        })
}

var downloadVideos = (info, callback) => {

    //Create path if it doesn't exist
    if (!fs.existsSync(`${downloadPath}/${info['episodeId']}/${info['game']}`)) {
        if (!fs.existsSync(`${downloadPath}/${info['episodeId']}`)) {
            logger.info(`Creating directory: ${downloadPath}/${info['episodeId']}`);
            fs.mkdirSync(`${downloadPath}/${info['episodeId']}`);
        }
        logger.info(`Creating directory: ${downloadPath}/${info['episodeId']}/${info['game']}`);
        fs.mkdirSync(`${downloadPath}/${info['episodeId']}/${info['game']}`);
    }

    var ctr = 0;
    var videoInfos = [];
    for (var video of info['videoData']) {

        var vidUrl = `https://www.youtube.com/watch?v=${video['videoId']}`;

        //Enable action after download complete
        var stream = fs.createWriteStream(`${downloadPath}/${info['episodeId']}/${info['game']}/${info['game']}_${video['videoId']}.mp4`);

        stream.on('close',
            () => {
                ctr++;

                //Finished with all
                if (ctr == info['videoData'].length) {
                    logger.info(`${info['videoData'].length} videos downloaded for ${info['game']}...`);
                    callback(null, info);
                    return;
                }
            }
        );

        download(vidUrl)
            .pipe(stream);
    }
}

module.exports = {
    getFootage
}