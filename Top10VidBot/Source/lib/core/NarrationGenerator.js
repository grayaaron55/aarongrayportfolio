let say = require('say');
let wavInfo = require('wav-file-info');

let ttsVoice = "Alex";
let ttsSpeed = 1.2;

var alert = (text) => {
    say.speak(text, ttsVoice, ttsSpeed, (err) => {
        if(!err){
            alert(text);
        }
        else{
            logger.err(`CANTI alert unsuccessful.`)
            logger.err(err);
        }
    })
}
var record = (text, path, callback) => {
    say.export(text, ttsVoice, ttsSpeed, path, callback);
}

var speak = (text) => {
    say.speak(text, ttsVoice, ttsSpeed);
}

var speak = (text, callback) => {
    say.speak(text, ttsVoice, ttsSpeed, callback);
}

var getFileInfo = (filename, callback) => {
    wavInfo.infoByFilename(`../audio/testing/${filename}.wav`, (err, info) => {
        callback(err, info);
        return;
    })
}

module.exports = {
    record,
    record,
    speak,
    speak,
    getFileInfo,
    alert
}