let themeGenerator = require('./ThemeGenerator');
let scriptGenerator = require('./ScriptGenerator');
let cuttingRoom = require('./cuttingroom');
let ytGrabber = require('./YTFootageGrabber');
let logger = require('./logger');
let narrate = require('./NarrationGenerator');
let utils = require('../utils/general');

let readline = require('readline');
let fs = require('fs');
let say = require('say');


let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Kickoff
var getTheme = () => {
    //Announce start of roll
    narrate.speak('Rolling');
    logger.info('ROLLING THEME');

    //Begin name roll process
    themeGenerator.rollName((err, data) => {
        if (err) {
            narrate.speak('You mightve hit the request limit again')
            logger.err(err);
            return;
        }
        // Making the computer yell at me so i dont have to monitor lol
        narrate.speak('Valid set found!');

        //Re-word any invalid names & reassign back to object
        var genreName = utils.crosswalkGenre(data['genre']['name']);
        data['genre']['name'] = genreName;

        var episodeId = `${data['genre']['id']}_${data['keyword']['id']}`;

        //Present topic for OK
        logger.info('~~~~~~~~~~~~~~~~~~~~~~~');
        logger.info(`Working Data: \n"${data['games'].length} ${data['keyword']['name']} ${data['genre']['name']}"\n`)
        logger.info(`Games:\n${getGameTitleString(data['games'])}`);
        logger.info('~~~~~~~~~~~~~~~~~~~~~~~');
        logger.info(`Beginning production on: Top ${data['games'].length} ${data['keyword']['name']} ${data['genre']['name']} Games`);

        // With theme and games selected, generate script
        scriptGenerator.generateScript(data, episodeId, (err, scriptData) => {
            if (!err) {
                logger.info('Script complete.');

                // On script complete, go to youtube to get videos for selected games
                ytGrabber.getFootage(data['games'], episodeId, (err, vidInfo) => {
                    if (err) {
                        if(err==`NOVID`){
                            // getTheme();
                        }
                        return;
                    }

                    logger.info(`${vidInfo.length} videos downloaded!`);

                    var postInfo = {
                        gameData: data,
                        episodeId: episodeId,
                        videoData: vidInfo,
                        scriptData: scriptData
                    }

                    logger.info(`Pre-production complete for Ep. ${episodeId}`);
                    beginProduction(postInfo);
                    return;
                })
            }
            else{
                //Handle script fail state
            }
        });
    })
}

/*{ 
    gameData: { 
        keyword: { 
            name: 'anarchism', 
            id: 7335 
        },
        genre: { 
            name: 'Beat em up', 
            id: 25 
        },
        games: [{
            'name': game['name'],
            'id': game['id'],
            'rating': game['rating'],
            'summary': game['summary']
        }] 
    },
    episodeId: '25_7335',
    videoData: { 
        videosInfo: [{ 
            game: 'Assassin\'s Creed IV: Black Flag',
            videoId: '548wz92w5Lg',
            title: 'Multiplayer Gameplay Trailer | Assassin\'s Creed 4 Black Flag [UK]',
            length: { s: '4', m: '2', h: 0 },
            channelName: 'Ubisoft' 
        }],
        game: 'TheLegendofZelda:ALinkBetweenWorlds',
        episodeId: '31_301' 
    },
    scriptData: {
        intro: '',
        outtro: '',
        entries: [{
            script: '',
            game: '',
            rank: 0
        }]
    }
 }
*/

//Begin production process with downloaded footage & script
var beginProduction = (data) => {
    logger.info(`Beginning Production...`);

    cuttingRoom.cutFootage(data);
    
    // Current end of line : (
    return;
}

//Just for dev display
var getGameTitleString = (games) => {
    var finalString = '';
    for (var game of games) {
        finalString += `${game['name']}\n`;
    }
    return finalString;
}

getTheme();