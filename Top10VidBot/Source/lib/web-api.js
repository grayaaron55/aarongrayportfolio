let req = require('request')
let logger = require('./logger');
let sleep = require('system-sleep')
let pluralize = require('pluralize')

let apiAddress = 'https://od-api.oxforddictionaries.com/api/v1'

// All of this is unused lol

var getSynonyms = (word, callback) => {
    var url = `${apiAddress}/entries/en/${word_id}/synonyms`;
}

// Oxford API wanted to put a limit on requests and i didnt want it

// let apiCreds = [
    // {//1500 left
    //     id: 'a7dfa6f6',
    //     key: '3a551e6aed50077a548a3084cce566ca'
    // },
//     {//3000 left
//         id: 'e586d227',
//         key: 'f9bc32118d6f0959e86e86397fe20f1a'
//     },
//     {
//         id: 'b5561132',
//         key: 'b4a5f86ac19ef3d85719253ba8b25164'
//     },
//     {
//         id: '5c4476a5',
//         key: '9b171a1e4a2fdfcaa7c401f6fe93d9fa'
//     },
//     {
//         id: 'd894fff8',
//         key: 'ea5da41fac7232d133a8f652b19896da'
//     },
//     {
//         id: 'db32906a',
//         key: 'b631daa1e5783896e97614551ae9618b'
//     },
//     {
//         id: '0d9e9cf5',
//         key: 'fe41bcbee6e6b46615560aeba11013bc'
//     },
//     {
//         id: '2635d085',
//         key: 'adf59f06d154c2d447ef070a91fe323b'
//     },
//     {
//         id: '8a9a7fdc',
//         key: 'c3ba35b4f9160453619e25a22cc43edc'
//     },
//     {
//         id: 'ef240648',
//         key: '03dbfb12d79c2d5154d10e508497020b'
//     },
//     {
//         id: 'f640a92a',
//         key: '1776328ffa32afeaa19cf28c95b50711'
//     }
// ]

var appId = apiCreds[2]["id"];
var dictApiKey = apiCreds[2]["key"]


let language = 'en' //for now...
let region = 'us'

let fillDefaults = true;

let DefaultGramFeatures = []

let DefaultRegisters = [
    "Slang", 
    "Personified",
    "Military_Slang", 
    "Informal", 
    "Derogatory", 
    "Dialect"
]

/*Domains To Test:*/
let DefaultDomains = [
    //"Zoology",    LOTS of big words
    "Wrestling",
    "Weapons",
    //"Video",
    //"War_Of_American_Independence",
    "Variety",
    //"Theatre",
    //"Technology",
    "Space",
    "Second_World_War", //** */
    "Royalty",  //I like it if can filter out names
    "Reptile",
    "Religion",
    "Racing",
    "Police",
    "Plumbing",
    "People",
    "Occult",
    "Naval",
    "Nautical",
    //"Napoleonic_Wars",
    //"Mythology",
    'Mountaineering',
    'Motoring',
    //'Motor_Racing',
    'Mining',
    'Military_History',
    'Military',
    // 'Medieval_History',//Remove names?
    // 'Medicine', //Remove jargon?
    // 'Martial_Arts',//Remove names
    'Mammal',
    // 'Leather',
    // 'Law',
    // 'Judaism',
    'Journalism',          //Rate all this funny or not & plug into
    'Jewellery',           //A neural net l o l? (too small...?)
    // 'Islam', //*
    // 'Jazz',
    'Instrument',
    'Hunting',
    // 'History',
    // 'Hinduism',
    // 'Heraldry',
    'Hairdressing',
    'Gymnastics',
    // 'Greek_History',
    'Goods_Vehicles',
    'Gaming',
    'Games',
    'Gambling',
    // 'French_Revolution',
    'Freemasonry',
    // 'Food',
    // 'First_World_War',
    'Film',
    'Farming',
    'English_Civil_War',
    'Electronics',
    'Dance',
    'Crime',
    'Computing',
    // 'Christian',
    'Chess',
    // 'Chemistry',
    'Cards',
    'Camping',
    // 'Buddhism',
    'Brewing',
    'Boxing',
    'Bowling',
    'Billiards',
    'Aviation',
    'Australian_Rules',
    'Art',
    'Archery',
    // 'American_Civil_War',
    'Alcoholic',
    'American_Football',
    'Air_Force',
]

var initStore = (store, callback) => {
    
    var defFilters = {
        domains: DefaultDomains,
        regions: [region],
        registers: DefaultRegisters,
        grammaticalFeatures: DefaultGramFeatures
    }

    store = {
        nounIds: [],
        usedNouns: [],
        adjIds: [],
        usedAdjectives: [],
        filters: defFilters
    }

    GetWordIDs("noun", defFilters, (err, data) => {
        if(err)
            return callback(err, null)
        
        store["noun_ids"] = data
        
        GetWordIDs("adjective", defFilters, (err, data) => {
            if(err)
                return callback(err, null)
            
            store["adj_ids"] = data

            return callback(null, store)
        })
    })
}

var GetWordIDs = (partOfSpeech, filters, callback) => {
    var mP = pluralize(partOfSpeech)
    logger.info(`Getting IDs for ${mP}`)
    var i = 0;
    var newList = []

    //Create Array of Arrays for each 
    var slicedDom = ArrayToSlicedArray(filters["domains"])
    var slicedRegis = ArrayToSlicedArray(filters["registers"])
    var slicedRegions = [['us']]
    var minutes = (slicedRegis.length*slicedDom.length*12)/60;
    logger.info(`Time estimate: ${minutes} minutes.`)
    
    for(var domainGroup of slicedDom){
        var domainField = domainGroup.length>0 ? ArrayToCsv(domainGroup) : "";

        for(var registerGroup of slicedRegis){
            var registerField = registerGroup.length>0 ? ArrayToCsv(registerGroup) : "";

            for(var regionGroup of slicedRegions){
                var regionField = regionGroup.length>0 ? ArrayToCsv(regionGroup) : "";


                var params = `/wordlist/${language}/lexicalCategory=${partOfSpeech};domains=${domainField};regions=${regionField};registers=${registerField}`
                
                logger.info("Making call for "+domainField+", "+registerField)
                req.get(apiAddress+params, {
                    headers:{
                        app_id: appId,
                        app_key: dictApiKey
                    }    
                },
                (err, resp, body) => {
                    if(err)
                        return callback(err, null)
                    
                    var results = JSON.parse(body)["results"];
                    if(results.length > 0){
                        for(var wordObj of results){
                            newList.push(wordObj["id"])
                        }
                        logger.info(`${results!=null ? results.length : 0} entries found.`)                        
                        logger.info(`Filters: \n--Category: ${partOfSpeech}\n--Domains: ${domainField}\n--Regions: ${regionField}\n--Registers: ${registerField}`)
                    }
                    else{
                        logger.info(`No entries found for D:${domainField}, Regis: ${registerField}`)
                    }

                    i++;
                    if(i==slicedDom.length*slicedRegis.length){                    
                        return callback(null, newList);
                    }
                })
                // logger.info("Sleeping for 12 seconds...")
                sleep(12000)
            }
        }
        i++;                    
    }
}
//Converts array to simple CSV ([1,2,3] to 1,2,3)
var ArrayToCsv = (array) => {
    if(array==null)
        return ""

    if(array.length>0){

        var csv = ""
        for(var elmnt of array){
            csv += elmnt+','
        }

        csv = csv.slice(0,-1)
        return csv
    }

    return ""
}

//Converts arrays to sliced groups inside a larger array
//Currently set to 1 bc the grouping wasnt doing right
var ArrayToSlicedArray = (array) => {
    var sliceSize = 1;

    if(array==null)
        return []
    var saved = 0;
    var sliced = []
    while(saved < array.length){
        var piece = array.slice(saved, saved+sliceSize)
        saved += piece.length

        sliced.push(piece)
    }
    sliced.unshift(["","",""])

    return sliced
}

var Lookup = (wordId, callback) => {
    var url = apiAddress +`/entries/${language}/${wordId}`

    req.get(url, {
        headers:{
            app_id: appId,
            app_key: dictApiKey
        }    
    }, (err, resp, body) => {
        if(err){
            logger.err("Error looking up "+wordId+"; "+err)
            callback(err, null)
        }
        // console.log(resp)
        // console.log(body)
        try{
            var resp = JSON.parse(body)["results"]
            if(resp.length>0)
                resp = resp[0]
            logger.info("Successfully retrieved info for "+wordId);
            return callback(null, resp)
        }
        catch(err){
            logger.err("Error parsing JSON body")
            return callback(err, null)
        }
    })
}

var getFilters = (filterType) => {
    req.get(apiAddress+`/${filterType}/${language}`, {
        headers:{
            app_id: appId,
            app_key: dictApiKey
        }    
    },
    (err, resp, body) => {
        console.log(body);
    })
}

var callCount = 501;
var acctIndex = 0;

var popEntries = (store, callback) => {

    var adjReady = false;
    var adjList = [];
    for(var adj of store["adj_ids"]){
        var word = adj.includes('_') ? adj.replace('_', ' ') : adj;
        if(word.includes('%')){
            continue
        }
        sleep(1200);        

        getSynonyms(adj, word, (err, synonyms, id, wordText) => {

            if(err){
                logger.err(err);
                return
            }

            logger.info(`Got ${synonyms.length} synonyms for ${wordText}`)            

            adjList.push({
                id: id,
                word: wordText,
                category: 'adj',
                synonyms: {
                    tier_one: synonyms,
                    tier_two: [],
                    tier_three: []
                }
            })

            if(id == store["adj_ids"][store["adj_ids"].length-1]){
                adjReady = true;
            }
        })
    }

    var nounList = [];
    for(var noun of store["noun_ids"]){
        var word = noun.includes('_') ? noun.replace('_', ' ') : noun;
        if(word.includes('%')){
            continue
        }
        sleep(1200)
        
        getSynonyms(noun, word, (err, synonyms, id, wordText) => {
            // callCount++;

            if(err){
                logger.err(err);
                return
            }

            logger.info(`Got ${synonyms.length} synonyms for ${wordText}`)

            nounList.push({
                id: id,
                word: wordText,
                category: 'noun',
                synonyms: {
                    tier_one: synonyms,
                    tier_two: [],
                    tier_three: []
                }
            })

            if(id == store["noun_ids"][store["noun_ids"].length-1] && adjReady){
                var resp = {
                    adj: adjList,
                    noun: nounList
                }
                callback(null, resp)
            }
        })

        
    }
}

// var getSynonyms = (word_id, callback) => {
//     var url = `${apiAddress}/entries/en/${word_id}/synonyms`;

//     req.get(url, {
//         headers:{
//             app_id: appId,
//             app_key: dictApiKey
//         }    
//     },(err, resp, body) => {
        
//         if(err){
//             logger.err('Error getting synonym for '+word_id)
//             callback(err, null)
//         }
//         body = JSON.parse(body)
//         var synonymList = [];
//         var results = body["results"];

//         for(var lexis of results){
//             for(var entries of lexis["lexicalEntries"]){
//                 for(var entry of entries["entries"]){
//                     if(entry["senses"]!=null){
//                         for(var senses of entry["senses"]){
//                             if(senses["synonyms"]!=null && senses["synonyms"].length!=0){
//                                 for(var syn of senses["synonyms"]){
//                                     synonymList.push(syn['text']);
//                                 }
//                             }
//                             if(senses["subsenses"]){
//                                 for(var subsense of senses["subsenses"]){
//                                     if(subsense["synonyms"]){
//                                         for(var syn of subsense["synonyms"]){
//                                             synonymList.push(syn["text"]);
//                                         }
//                                     }
//                                 }
//                             }
//                         }
//                     }
//                 }
//             }
//         }

//         if(synonymList.length>0)
//             callback(null, synonymList);
//         else
//             callback('No synonyms found.', null)
//     })
// }

var getSynonymsAndAntonyms = (word_id, callback) => {
    var url = `${apiAddress}/entries/en/${word_id}/synonyms;antonyms`;

    req.get(url, {
        headers:{
            app_id: appId,
            app_key: dictApiKey
        }    
    },(err, resp, body) => {
        
        if(err){
            logger.err('Error getting synonym for '+word_id)
            callback(err, null, null)
        }
        body = JSON.parse(body)
        var synonymList = [];
        var antonymList = [];
        var results = body["results"];

        for(var lexis of results){
            for(var entries of lexis["lexicalEntries"]){
                for(var entry of entries["entries"]){
                    if(entry["senses"]!=null){
                        for(var senses of entry["senses"]){
                            if(senses["synonyms"]!=null && senses["synonyms"].length!=0){
                                for(var syn of senses["synonyms"]){
                                    synonymList.push(syn['text']);
                                }
                            }
                            if(senses["antonyms"]!=null && senses["antonyms"].length!=0){
                                for(var ant of senses["antonyms"]){
                                    antonymList.push(ant['text']);
                                }
                            }
                            if(senses["subsenses"]){
                                for(var subsense of senses["subsenses"]){
                                    if(subsense["synonyms"]){
                                        for(var syn of subsense["synonyms"]){
                                            synonymList.push(syn["text"]);
                                        }
                                    }
                                    if(subsense["antonyms"]){
                                        for(var ant of subsense["antonyms"]){
                                            antonymList.push(ant["text"]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        callback(null, synonymList, antonymList);
    })
}

module.exports = {
    initStore,
    Lookup,
    popEntries,
    getSynonymsAndAntonyms
}
//Tests

getSynonyms('academic', (err, resp) => {
    if(err){
        console.log('fucked')
    }

    console.log(resp)
})

// lexicalcategories, registers, domains, regions, grammaticalFeatures, 
// WordListsByCategory('Adjective');

// lookup("Ace", (err, data) => {
//     console.log("!!!")
//     console.log(data)
// })

//Remove Proper from gramFeatures?

//Lexical: Noun, Adjective
//Registers: Vulgar_Slang, University_Slang, Slang, Proverb, Prison Slang, Police Slang, Personified
            //Military_Slang, Journalists_Slang, Informal, Derogatory, Black_English, Dialect
//GramFeat: Inerrogative, Possessive, Relative, Attributive, Predicative,