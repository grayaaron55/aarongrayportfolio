Aaron Gray Portfolio 04/2018
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Top 10 Vid Bot - Node/JS
--------------
Project aimed at creating a generative system to create satirical Top 10 game-themed videos in the vein of
WatchMojo YouTube videos. Autogenerates a theme, script, gathers footage from YT, generates narration. In 
latest version held ability to cut & splice footage via ffmpeg, but can't find any spliced clips to demo. 
Code is still intact for that functionality but with API keys removed.

Endearingly named "CantiBot" in development. Some dev notes included in folder.
--------------

Crypto Bot (Typescript)
--------------
Project aimed at creating a swarm of bots to watch multiple cryptocurrency markets & exchanges simultaneously
and automating buy/sell orders. Was able to get bots on a 3-minute increment to a 48% winrate before I lost
time to work on the project consistently. First time experimenting with WebSocket API, turned out to be 
pretty similar to Observables in the Angular 2+ framework.

Project is a bit disorganized but, with it being a solo hobby experiment, didn't anticipate the size of it.
--------------
