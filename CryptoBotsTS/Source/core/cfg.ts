import * as loadFile from 'load-json-file';
import * as process from 'process';

let debugCfgPath = './cfg/cfg_debug.json'
let swarmCfgPath = './cfg/cfg_swarm.json';
let acctCfgPath = './cfg/cfg_acct.json';
let mktCfgPath = './cfg/cfg_market.json';

export interface DebugConfig{
    tickDataWriteEnabled: boolean;
    trainingMode: boolean;
    incrementCodes: string[];
    runLabel: string;
    reportRate: number;
    reportRateIncrement: number;
    trainingDataDateStamp: string;
}

export interface MarketConfig{
    //Exchanges
    exchangeCodes: string[];

    //Primary markets (BTC for now)
    primaryMarkets: string[];
    
    //Market lists 
    mktList_Liqui: string[];
    mktList_Binance: string[];
}

export interface SwarmConfig{
    //Agents
    agent_replicationEnabled: boolean;
    agent_drawEnabled: boolean;

    //Indicators
    ind_bollingerMultiplier: number;
    ind_lowerStochasticLimit: number;
    ind_upperStochasticLimit: number;
    ind_stochasticBarrier: number;
    ind_stochasticLookbackPeriod: number;
    ind_stochasticSmoothingPeriod: number;

    tck_cacheSize: number;

    //Verifications
    vfy_buyVerifications: number;
    vfy_verificationSlipTolerance: number;
    vfy_bufferSlipTolerance: number;
    vfy_rampSlipTolerance: number;
    vfy_buyinWaitTolerance: number;
}

export interface AcctConfig{
    //Coinigy
    coinigy_ApiKey: string;
    coinigy_ApiSecret: string;
    coinigy_ApiUrl: string;
}

export class Config{
    constructor(
        public debug_cfg: DebugConfig,
        public mkt_cfg: MarketConfig,
        public acct_cfg: AcctConfig, 
        public swarm_cfg: SwarmConfig){}
}

export function getConfig(): Config{
    let debug_cfg = loadFile.sync(debugCfgPath);
    let mkt_cfg = loadFile.sync(mktCfgPath);
    let acct_cfg = loadFile.sync(acctCfgPath);
    let swarm_cfg = loadFile.sync(swarmCfgPath);

    return new Config(debug_cfg, mkt_cfg, acct_cfg, swarm_cfg);
}