export enum MessageType{
    Log,
    Error,
    Info,
    Detail,
    Debug
}

let acceptedReportTypes = [
    MessageType.Log,
    MessageType.Error,
    MessageType.Debug
]

export function report(agentId: string, messageType: MessageType, message: string, header: string=''){
    var logString = `:HARVEST:`;
    switch(messageType){
        case MessageType.Log:
            logString += `LOG:`;
            break;
        case MessageType.Error:
            logString += `ERROR:`;
            break;
        case MessageType.Info:
            logString += `INFO:`;
            break;
        case MessageType.Debug:
            logString += `DEBUG-----------------\n`
        default:
            break;
    }
    logString += `${agentId}:${header}:\n`;
    logString += message;

    for(var type of acceptedReportTypes){
        if(messageType==type){
            console.log(logString);
        }
    }
}