import * as moment from "moment";
import * as repeat from "repeat-time";
import * as loadFile from 'load-json-file';
import * as fs from 'fs';

import { AgentStatus } from "../../models/swarm/enums/WatcherStatus";
import { Tick } from "../../models/chart-entities/Tick";
import { watchChannel } from "../../tools/SocketClusterTools";
import { MarketOrder } from "../../models/tools/api/dbmodels/MarketOrder";
import { getConfig, Config, AcctConfig, DebugConfig, SwarmConfig, MarketConfig } from '../../cfg';
import { BollingerZone } from "../../models/chart-entities/BollingerZone";
import { StochasticZone } from "../../models/chart-entities/StochasticZone";
import { report, MessageType } from "./MessageHandler";
import { addTickToRunner, streamToTrainer } from "../../../index";

var cfg = getConfig();

export class TickAgent {
    //Agent info
    id: string;
    market: string;

    //Live info
    status: AgentStatus;

    //Stores orders until enough time has passed to create a Tick
    lastTickTime: Date;
    currentTermOpenPrice: any = null;
    currentTermHighPrice: any = null;
    currentTermLowPrice: any = null;

    cachedTick: Tick;
    cachedOrder: MarketOrder | undefined;

    successfulVerifications: number = 0;
    verificationSlips: number = 0;
    bufferSlips: number = 0;
    rampSlips: number = 0;
    buyinWaitTicks: number = 0;

    buyinPrice: number;

    stat_verdict: string;
    stat_diff: number;

    constructor(public exchangeCode: string, public primaryCurrency: string, public secondaryCurrency: string, public tickCache: Tick[], public incrementCode: string, public agentId=0) {
        this.market = `${primaryCurrency}/${secondaryCurrency}`;
        this.id = `${exchangeCode}:${this.market}:${incrementCode}:${agentId}`;
        
        //If ticks in given cache, use latest as cachedTick
        if(tickCache.length>0)
            this.cachedTick = tickCache[tickCache.length-1];
        //Else make a new one
        else
            this.cachedTick = new Tick(0,0,0,0,tickCache);

        //If appropriate amount in cache
        if (this.tickCache.length >= cfg.swarm_cfg.tck_cacheSize) {
            this.setStatus(AgentStatus.WaitingForSignal);
        }
        else {
            this.setStatus(AgentStatus.Priming);
        }

        if(cfg.debug_cfg.trainingMode==true){
            this.initializeTrainingMode();
        }
        else{
            this.initializeTickMaker();
            this.watchOrderChannel();
        }
    }

    initializeTrainingMode(){
        report(this.id, MessageType.Log, `Initializing training mode`);

        let fileName = `./trainer/channelData/${cfg.debug_cfg.runLabel}_${cfg.debug_cfg.trainingDataDateStamp}/${this.exchangeCode}${this.primaryCurrency}${this.incrementCode}.json`;
        
        //If training data doesn't exist, end run
        if(!fs.existsSync(fileName)){
            report(this.id, MessageType.Error, `No training data found at ${fileName}`, `Training Error`);
            return;
        }
        
        var trainingData = loadFile.sync(fileName).history;

        for(var tick of trainingData){
            var newTick = this.cloneTick(tick);
            this.processTick(newTick);
        }
    }

    initializeTickMaker(){
        //Initialize repeating tick processor
        repeat(this.incrementCode, () => {
            if(this.cachedOrder!=null){
                var newTick = this.createNewTick(this.cachedOrder);
                this.processTick(newTick);
            }
        });
    }

    //Watch socket for new order info
    watchOrderChannel(): void {
        watchChannel(`TRADE-${this.exchangeCode}--${this.primaryCurrency}--${this.secondaryCurrency}`,
            (err: any, newOrder: MarketOrder) => {
                if (err) {
                    report(this.id, MessageType.Error, err, `Watch Order Channel`);
                    return;
                }
                report(this.id, MessageType.Detail, `Market Order ${newOrder.tradeid} received`, `Watch Order Channel`);
                this.processNewOrder(newOrder);
            }
        );
    }

    processNewOrder(order: MarketOrder){
        report(this.id, MessageType.Detail, `Processing Market Order ${order.tradeid}`, `Process New Order`);

        //Cache info for tick
        this.cachedOrder = order;

        //If no recorded High || new price > current recorded high, take new price as term High
        if(this.currentTermHighPrice===null || order.price > this.currentTermHighPrice){
            this.currentTermHighPrice = order.price;
        }
        //If no recorded Low || new price > current recorded low, take new price as term Low
        else if(this.currentTermLowPrice===null || order.price < this.currentTermLowPrice){
            this.currentTermLowPrice = order.price;
        }
    }

    cloneTick(tick: Tick): Tick{
        report(this.id, MessageType.Info, `Cloning Tick`, `Create New Tick`);

        var newTick = tick;

        if(this.tickCache.length >= cfg.swarm_cfg.tck_cacheSize){
            var removed = this.tickCache.shift();
        }

        this.tickCache.push(newTick);

        //Stream data to trainer
        if(cfg.debug_cfg.tickDataWriteEnabled==true){
            //Get market string for file path
            var trimmedCode = this.market.split('/')[0];

            //Send Tick data to trainer to save
            streamToTrainer(newTick, this.exchangeCode, trimmedCode, this.incrementCode);
        }

        //If Priming and enough results have been found, change status
        if(this.status == AgentStatus.Priming && this.tickCache.length>=cfg.swarm_cfg.tck_cacheSize){
            report(this.id, MessageType.Log, `Priming complete. (${this.tickCache.length}/${cfg.swarm_cfg.tck_cacheSize})`, `Create New Tick`);
            this.setStatus(AgentStatus.WaitingForSignal);
        }

        return newTick;
    }

    createNewTick(closeOrder: MarketOrder): Tick{
        report(this.id, MessageType.Info, `Creating new Tick - MO#${closeOrder.tradeid}`, `Create New Tick`);

        var newTick = new Tick(this.currentTermOpenPrice, closeOrder.price, this.currentTermHighPrice, this.currentTermLowPrice, this.tickCache);

        if(this.tickCache.length >= cfg.swarm_cfg.tck_cacheSize){
            var removed = this.tickCache.shift();
        }

        this.tickCache.push(newTick);

        //Stream data to trainer
        if(cfg.debug_cfg.tickDataWriteEnabled==true){
            //Get market string for file path
            var trimmedCode = this.market.split('/')[0];

            //Send Tick data to trainer to save
            streamToTrainer(newTick, this.exchangeCode, trimmedCode, this.incrementCode);
        }

        this.currentTermOpenPrice = null;
        this.currentTermHighPrice = null;
        this.currentTermLowPrice = null;

        if(this.status == AgentStatus.Priming && this.tickCache.length>=cfg.swarm_cfg.tck_cacheSize){
            report(this.id, MessageType.Log, `Priming complete. (${this.tickCache.length}/${cfg.swarm_cfg.tck_cacheSize})`, `Create New Tick`);
            this.setStatus(AgentStatus.WaitingForSignal);
        }
        return newTick;
    }

    //This is where buying rules will be implemented
    processTick(tick: Tick): void {
        var logHeader = `Process Tick`;
        
        //State machine for overall Agent status
        switch (this.status) {

            case AgentStatus.Priming:
                report(this.id, MessageType.Info, `Processing tick with Priming rules`, logHeader);
                break;

            //Watching market waiting for signal to consider buy
            case AgentStatus.WaitingForSignal:
                report(this.id, MessageType.Info, `Processing Tick with 'Waiting For Signal' rules`, logHeader);
                this.watchForSignal(tick);
                break;

            //Watching ticks for a buy signal
            case AgentStatus.WatchingForBuy:
                report(this.id, MessageType.Info, `Processing Tick with 'Watching For Buy' rules`, logHeader);
                this.watchForBuy(tick);
                break;

            case AgentStatus.Verifying:
                report(this.id, MessageType.Info, `Processing Tick with Verifying rules`, logHeader);
                this.verify(tick);
                break;

            case AgentStatus.Holding:
                report(this.id, MessageType.Info, `Processing Tick with Holding rules`, logHeader);
                this.watchForSell(tick);
                break;

            default:
                report(this.id, MessageType.Info, `No rules selected for processing Tick`, logHeader);
                break;
        }

        //Cache this tick for next round
        this.cachedTick = tick;
    }

    watchForSignal(tick: Tick) {
        
        switch (tick.bollingerZone) {

            case BollingerZone.Under:
                report(this.id, MessageType.Info, `Bollinger zone - Under; <Close: ${tick.closePrice}, Lower: ${tick.lowerBollingerLimit}>`, `Watch For Signal`);

                switch (this.cachedTick.bollingerZone) {
                    
                    case BollingerZone.Over:
                        report(this.id, MessageType.Info, `Bollinger zone: Over => Under - Pass`, `Watch For Signal`);
                        break;

                    case BollingerZone.Inside:
                        report(this.id, MessageType.Info, `Bollinger zone: Inside => Under`, `Watch For Signal`);
                        this.setStatus(AgentStatus.WatchingForBuy);
                        break;
                    
                    case BollingerZone.Under: 
                        report(this.id, MessageType.Info, `Bollinger zone: Under => Under - Pass`, `Watch For Signal`);
                        break;

                    default:
                        report(this.id, MessageType.Info, `Bollinger zone: default => Under - Pass`, `Watch For Signal`);
                        break;
                }
                break;

            case BollingerZone.Over:
                report(this.id, MessageType.Info, `Bollinger zone -  Over; <Close: ${tick.closePrice}, Upper: ${tick.upperBollingerLimit}>`, `Watch For Signal`);

                switch (this.cachedTick.bollingerZone) {

                    case BollingerZone.Over:
                        report(this.id, MessageType.Info, `Bollinger zone: Over => Over - Pass`, `Watch For Signal`);
                        break;

                    case BollingerZone.Inside:
                        report(this.id, MessageType.Info, `Bollinger zone - Inside => Over - Pass`, `Watch For Signal`);
                        break;

                    case BollingerZone.Under:
                        report(this.id, MessageType.Info, `Bollinger zone: Over => Under - Over`, `Watch For Signal`);
                        break;
                    
                    default:
                        break;
                }
                break;
            
                case BollingerZone.Inside:
                    report(this.id, MessageType.Info, `Bollinger zone - Inside; <Lower: ${tick.lowerBollingerLimit}, Close: ${tick.closePrice}, Upper: ${tick.upperBollingerLimit}>`, `Watch For Signal`);

                    switch (this.cachedTick.bollingerZone) {

                        case BollingerZone.Over:
                            report(this.id, MessageType.Info, `Bollinger zone: Over => Inside - Pass`, `Watch For Signal`);
                            break;

                        case BollingerZone.Inside:
                            report(this.id, MessageType.Info, `Bollinger zone: Inside => Inside - Pass`, `Watch For Signal`);
                            break;

                        case BollingerZone.Under:
                            report(this.id, MessageType.Info, `Bollinger zone - Under => Inside - Pass`, `Watch For Signal`);
                            break;
    
                        default:
                            break;
                    }
                    break;

            default:
                report(this.id, MessageType.Info, `Bollinger zone - Pass`, `Watch For Signal`);
                break;
        }
    }

    watchForBuy(tick: Tick){

        //Tick is inside 
        switch (tick.bollingerZone) {

            //Tick is within bollinger bands
            case BollingerZone.Inside:
                report(this.id, MessageType.Info, `Tick within Bollinger bands <${tick.lowerBollingerLimit}, ${tick.closePrice}, ${tick.upperBollingerLimit}>`, `Watch For Buy`);

                //Judge tick based on momentum
                switch (tick.stochasticZone) {

                    //If tick is undersold
                    case StochasticZone.Under:
                        report(this.id, MessageType.Log, `Tick within Bollinger Bands && Undersold - Verifying; <K: ${tick.momentumK} D: ${tick.momentumD} Sig: ${cfg.swarm_cfg.ind_lowerStochasticLimit}>`, `Watch For Buy`);
                        this.setStatus(AgentStatus.Verifying);
                        break;

                    default:
                        report(this.id, MessageType.Log, `Tick within Bollinger Bands && NOT Undersold - Back to Waiting; <K: ${tick.momentumK} D: ${tick.momentumD} Sig: ${cfg.swarm_cfg.ind_lowerStochasticLimit}>`, `Watch for Buy`);
                        this.setStatus(AgentStatus.WaitingForSignal);
                        break;
                }
                break;

            default:
                report(this.id, MessageType.Info, `Tick outside Bollinger Bands; <${tick.lowerBollingerLimit}, ${tick.closePrice}, ${tick.upperBollingerLimit}>`, `Watch For Buy`);
                break;
        }
    }

    verify(tick: Tick){
        var momentumIncreased = tick.momentumD > this.cachedTick.momentumD;
        
        //Momentum increase check
        if(!momentumIncreased){
            report(this.id, MessageType.Log, `Momentum failed to increase - Checking slip threshold; <${tick.momentumD}/${this.cachedTick.momentumD}>`, `Verify`);
            this.registerVerificationSlip();
            return;
        }


        if(tick.stochasticZone == StochasticZone.Under){
            report(this.id, MessageType.Info, `Tick detected Under stochastic bars - checking buyin wait threshold`, `Verify`);
            this.registerBuyInWait();

            //Order was canceled
            if(this.status != AgentStatus.Verifying){
                report(this.id, MessageType.Info, `Agent is no longer verifying - Exiting loop;`, `Verify`);
                return;
            }
        }
        // else if(tick.stochasticZone == StochasticZone.Buffer){
        // }

        this.successfulVerifications++;
        
        if(this.successfulVerifications >= cfg.swarm_cfg.vfy_buyVerifications){
            report(this.id, MessageType.Log, `Required Verifications met (${this.successfulVerifications}/${cfg.swarm_cfg.vfy_buyVerifications}) - Comitting Buy order`, `Verify`);
            this.buy(tick);
        }
    }

    watchForSell(tick: Tick){
        var momentumIncreased = tick.momentumD > this.cachedTick.momentumD;
        //Monitor momentum
        switch(tick.stochasticZone){

            case StochasticZone.Under:
                report(this.id, MessageType.Info, `Tick is below lower Stochastic limit - Selling`, `Watch For Sell`);
                this.sell(tick);
                break;

            case StochasticZone.Buffer:
                if(!momentumIncreased){
                    report(this.id, MessageType.Info, `Momentum slip detected in Buffer zone - Checking Tolerance`, `Watch For Sell`);
                    this.registerBufferSlip(tick);
                }
                break;

            case StochasticZone.Ramp:
                if(!momentumIncreased){
                    report(this.id, MessageType.Info, `Momentum slip detected in Ramp zone - Checking Tolerance`, `Watch For Sell`);
                    this.registerRampSlip(tick);
                }
                break;

            case StochasticZone.Over:
                //Hold
                report(this.id, MessageType.Info, `Momentum above upper Stochastic limit - Holding`, `Watch For Sell`);
                break;

            default:
                break;
        }
    }

    registerVerificationSlip(){
        if(this.verificationSlips < cfg.swarm_cfg.vfy_verificationSlipTolerance){
            this.verificationSlips++;
            return;
        }
        //If too many slips, cancel order
        else{
            report(this.id, MessageType.Log, `Maximum Verification Slips exceeded - Cancelling`, `Register Verification Slip`);
            this.cancelVerification();
        }
    }

    registerBuyInWait(){
        if(this.buyinWaitTicks >= cfg.swarm_cfg.vfy_buyinWaitTolerance){
            report(this.id, MessageType.Log, `Maximum BuyIn Wait ticks exceeded - Cancelling`, `Register BuyIn Wait`);
            this.cancelVerification();
        }
        else{
            this.buyinWaitTicks++;
        }
    }

    registerBufferSlip(tick: Tick){
        if(this.bufferSlips >= cfg.swarm_cfg.vfy_bufferSlipTolerance || this.cachedTick.stochasticZone!=tick.stochasticZone){
            report(this.id, MessageType.Log, `Maximum Buffer Slips exceeded || Momentum slipped across border - Selling`, `Register Buffer Slip`);
            
            //Saving throw
            if(this.buyinPrice >= tick.closePrice){
                return;
            }
            this.sell(tick);
        }
        else{
            this.bufferSlips++;
        }
    }

    registerRampSlip(tick: Tick){
        if(this.rampSlips >= cfg.swarm_cfg.vfy_rampSlipTolerance || this.cachedTick.stochasticZone==StochasticZone.Over){
            report(this.id, MessageType.Log, `Maximum Ramp Slips exceeded || Momentum slipped across border - Selling`, `Register Ramp Slip`);
            
            //Saving throw
            if(this.buyinPrice >= tick.closePrice){
                return;
            }
            this.sell(tick);
        }
        else{
            this.rampSlips++;
        }
    }

    getTickStatus(tick: Tick): TickStatus {
        //Closing price is within Bollinger limits
        if (tick.closePrice <= tick.upperBollingerLimit && tick.closePrice >= tick.lowerBollingerLimit) {
            return TickStatus.WithinDeviation;
        }
        //Closing price is below Bollinger limits (undersold)
        else if (tick.closePrice > tick.upperBollingerLimit) {
            return TickStatus.Over;
        }
        //Closing price is above Bollinger limits (overbought)
        else {
            return TickStatus.Under;
        }
    }

    setStatus(status: AgentStatus): void {
        report(this.id, MessageType.Info, `Changing state: ${this.status} => ${status}`, `Set Status`);
        this.status = status;
    };

    cancelVerification(){
        report(this.id, MessageType.Log, `Cancelling verification process. Returning to 'Waiting For Signal' state`, 'Cancel Verification');
        this.verificationSlips = 0;
        this.buyinWaitTicks = 0;
        this.setStatus(AgentStatus.WaitingForSignal)
    }

    //Place purchase order
    buy(tick: Tick): void { 

        report(this.id, MessageType.Log, `Mock buy order complete; Price: ${tick.closePrice}`, `RESULT`);
        // console.log(tick);

        this.buyinPrice = tick.closePrice;
        this.setStatus(AgentStatus.Holding);

        //Spawn new agent to watch feed
        if(cfg.swarm_cfg.agent_replicationEnabled){
            var newAgent = new TickAgent(this.exchangeCode, this.primaryCurrency, this.secondaryCurrency, this.tickCache, this.incrementCode, (this.agentId+1))
            addTickToRunner(newAgent);
        }
    };

    //Place sell order
    sell(tick: Tick): void {

        this.setStatus(AgentStatus.Finished);

        //Sell logic
        report(this.id, MessageType.Log, "Mock sell!", `RESULT`)
        var diff = tick.closePrice - this.buyinPrice;
        this.stat_verdict = diff>0 ? 'WIN' : 
            diff==0 ? 'DRAW' : 'LOSS';
        
            if(this.stat_verdict=='DRAW' && !cfg.swarm_cfg.agent_drawEnabled){
                report(this.id, MessageType.Log, "Trying to sell a DRAW. Cancelling sell command");
                this.setStatus(AgentStatus.Holding);
                return;
            }
            else{
                this.stat_diff = diff;
                report(this.id, MessageType.Log,`Buy In: ${this.buyinPrice}  Sell At: ${tick.closePrice}  Difference: ${diff}  Verdict: ${this.stat_verdict}`, `RESULT`)
            }
    };
}

export enum TickStatus {
    Under,
    Over,
    WithinDeviation
}

enum WagerStatus{
    Win,
    Loss,
    Draw
}