import { MarketOrder } from "./MarketOrder";

export interface MarketHistory{
    exchangeCode: string;
    primaryCurrencyCode: string;
    secondaryCurrencyCode: string;
    history: MarketOrder[];
}