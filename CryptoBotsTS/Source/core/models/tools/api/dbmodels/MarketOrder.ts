import { MarketOrderType } from "./enums/MarketOrderType";

export class MarketOrder{
    market_history_id: number;
    exchange: string;
    marketid: number;
    label: string;
    tradeid: string;
    time: Date;
    price: number;
    quantity: number;
    total: number;
    timestamp: string;
    time_local: Date;
    type: string;
    get orderType(): MarketOrderType | null{
        switch(this.type){
            case "SELL":
                return MarketOrderType.Buy;
            case "BUY":
                return MarketOrderType.Sell;
            default: 
                return null;
        }
    }
}