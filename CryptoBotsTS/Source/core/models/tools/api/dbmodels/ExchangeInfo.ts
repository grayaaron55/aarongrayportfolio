export interface ExchangeInfo{
    exchangeId: number;
    name: string;
    code: string;
    fee: number;
    tradeEnabled: boolean;
    balanceEnabled: boolean;
    url: string;
}