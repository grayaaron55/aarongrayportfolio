export interface AccountInfo{
    auth_id: number;
    auth_key: string;
    auth_optional1: string;
    auth_nickname: string;
    exch_name: string;
    auth_secret: string;
    auth_updated: Date;
    auth_active: boolean;
    auth_trade: boolean;
    exch_trade_enabled: boolean;
    exch_id: number;
}