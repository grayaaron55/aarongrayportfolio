export interface OrderRequest{
    //Get from account
    authId: number;
    //Get from enum
    exchangeId: number;
    //Get from enum
    marketId: number;
    //Get from enum
    orderTypeId: number;
    //???
    priceTypeId: number;
    //What it says
    limitPrice: number;
    //What it says
    orderQuantity: number;

}