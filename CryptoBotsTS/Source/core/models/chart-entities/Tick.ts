import * as moment from "moment";

import { TickType } from '../swarm/enums/TickType';
import { getConfig, Config, AcctConfig, DebugConfig, SwarmConfig, MarketConfig } from '../../cfg';
import { StochasticZone } from './StochasticZone';
import { BollingerZone } from './BollingerZone';

var cfg: Config = getConfig();

export class Tick{
    //Tick ID string
    id: string;
    dateStamp: Date;

    //Reported price data given from agent
    openPrice: number;
    closePrice: number;
    highPrice: number;
    lowPrice: number;

    //Enums
    stochasticZone: StochasticZone;
    bollingerZone: BollingerZone;
    tickType: TickType;

    //Stats
    currentMovingAverage: number;
    upperBollingerLimit: number;
    lowerBollingerLimit: number;
    momentumK: number;
    momentumD: number;

    //(OpenPrice, ClosePrice, HighestPrice, LowestPrice, PreviousTicks)
    constructor(_open: number, _close: number, _hi: number, _lo: number, tickCache: Tick[]){
        
        //Form ID
        this.id = `${moment().toISOString()}-${_open}-${_close}`;

        //Record data passed in from Agent
        this.openPrice = _open;
        this.closePrice = _close;
        this.highPrice = _hi;
        this.lowPrice = _lo;

        //Record date and populate stat fields
        this.dateStamp = new Date();
        this.populateStats(tickCache);
    }

    //Calculate & populate stat fields for Agent analysis
    populateStats(tickCache: Tick[]): void{
        
        //Calculate moving average of the stock
        this.currentMovingAverage = this.getAveragePrice(tickCache);
        
        //Calculate the High/Low Bollinger band values
        this.getBollingerLimits(tickCache);

        //Calculate momentum K & D from tickCache
        this.getStochasticLevels(tickCache);
    };

    //Calculate upper & lower bollinger band values
    getBollingerLimits(previousTicks: Tick[]): void{
        
        var sampleSize: number = previousTicks.length;
        var sampleMean = this.getAveragePrice(previousTicks);
        var summation: number = 0;

        for(var tick of previousTicks){
            var xMinusX = tick.closePrice - sampleMean;
            var squared = xMinusX*xMinusX;
            summation += squared;
        }

        var stdDeviation = Math.sqrt(summation / sampleSize);
        this.lowerBollingerLimit = sampleMean - (stdDeviation * cfg.swarm_cfg.ind_bollingerMultiplier);
        this.upperBollingerLimit = sampleMean + (stdDeviation * cfg.swarm_cfg.ind_bollingerMultiplier);

        //Assign proper enum for Agent to read from
        this.setBollingerZone();
    }

    //Set enum value for Agent analysis
    setBollingerZone(){

        //Closing price underneath lower band
        if(this.closePrice < this.lowerBollingerLimit){
            this.bollingerZone = BollingerZone.Under;
        }
        //Closing price above upper band
        else if(this.closePrice > this.upperBollingerLimit){
            this.bollingerZone = BollingerZone.Over;
        }
        //Closing price within bands
        else{
            this.bollingerZone = BollingerZone.Inside;
        }
    }

    //Calculate current stochastic levels based on previous ticks
    getStochasticLevels(previousTicks: Tick[]): void{

        //If we haven't gathered enough data then input defaults and pass
        if(previousTicks.length<cfg.swarm_cfg.tck_cacheSize){
            this.momentumD = 0;
            this.momentumK = 0;
            return;
        }

        //Calculate vars for Stochastic formula
        var C = this.closePrice;
        var L = this.getLookbackLowest(previousTicks);
        var H = this.getLookbackHighest(previousTicks);

        //Stochastic value formula
        this.momentumK = ((C - L)/(H-L)) * 100;

        //Slice off the last n elements of array & get D
        var arrayLookbackIndex = cfg.swarm_cfg.ind_stochasticSmoothingPeriod-1;
        this.momentumD = this.calculateStochasticAverage(previousTicks.slice(-arrayLookbackIndex), this.momentumK);
    }

    //Set enum for Agent analysis
    setStochasticZone(){
        
        //Retrieve values from config
        let lowerLimit = cfg.swarm_cfg.ind_lowerStochasticLimit;
        let upperLimit = cfg.swarm_cfg.ind_upperStochasticLimit;
        let barrier = cfg.swarm_cfg.ind_stochasticBarrier;

        //D underneath stochastic zone
        if(this.momentumD < lowerLimit){
            this.stochasticZone = StochasticZone.Under;
        }
        //D above lower limit but below barrier (Buffer)
        else if(this.momentumD >= lowerLimit && this.momentumD <= barrier){
            this.stochasticZone = StochasticZone.Buffer;
        }
        //D above barrier but below upper limit (Ramp)
        else if(this.momentumD <= upperLimit && this.momentumD > barrier){
            this.stochasticZone = StochasticZone.Ramp;
        }
        //D above upper limit
        else if(this.momentumD > upperLimit){
            this.stochasticZone = StochasticZone.Over;
        }
        //Default
        else{
            this.stochasticZone = StochasticZone.Under;
        }
    }

    //Get the SMA of momentumK for the n smoothing ticks
    calculateStochasticAverage(ticks: Tick[], currentStochasticValue: number): number{
        
        var valueSum: number = 0;

        for(var tick of ticks){
            valueSum += tick.momentumK;
        }
        valueSum += currentStochasticValue;
        return valueSum / (ticks.length+1);
    }

    //Get lowest closing price in lookback period
    getLookbackLowest(ticks: Tick[]): number{
        
        var lowestClosingPrice: any = null;

        for(var tick of ticks){
            if(lowestClosingPrice===null){
                lowestClosingPrice = tick.lowPrice;
            }
            if(tick.lowPrice < lowestClosingPrice){
                lowestClosingPrice = tick.lowPrice;
            }
        }
        
        //If current is less
        if(this.lowPrice < lowestClosingPrice){
            return this.lowPrice;
        }
        //If tick array was empty
        if(lowestClosingPrice==null){
            return this.closePrice;
        }
        return lowestClosingPrice;
    }

    getLookbackHighest(ticks: Tick[]): number{
        
        var highestClosingPrice: number = 0;
        for(var tick of ticks){
            if(highestClosingPrice==0){
                highestClosingPrice = tick.highPrice;
            }
            if(tick.highPrice > highestClosingPrice){
                highestClosingPrice = tick.highPrice;
            }
        }

        if(this.highPrice > highestClosingPrice){
            return this.highPrice;
        }

        if(highestClosingPrice==0){
            return this.closePrice;
        }

        return highestClosingPrice;
    }

    getAveragePrice(array: any[]): number{

        var sampleMeanPrice: number = 0;
        for(var tick of array){
            sampleMeanPrice += tick.closePrice;
        }

        sampleMeanPrice += this.closePrice;
        sampleMeanPrice = sampleMeanPrice / (array.length+1);

        return sampleMeanPrice;
    }

    setTickType(){
        
        if(this.openPrice > this.closePrice){
            this.tickType = TickType.Bear;
        }
        else if(this.openPrice < this.closePrice){
            this.tickType = TickType.Bull;
        }
        else {
            this.tickType = TickType.Split;
        }   
    }
}