export enum AgentStatus{
    Priming,
    WaitingForSignal,
    WatchingForBuy,
    Verifying,
    Holding,
    Finished
}