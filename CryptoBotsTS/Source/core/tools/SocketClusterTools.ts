import * as socketCluster from 'socketcluster-client';
import { getConfig, Config, AcctConfig, DebugConfig, SwarmConfig, MarketConfig } from '../cfg';
var cfg: Config = getConfig();
var apiCreds = {
    "apiKey": cfg.acct_cfg.coinigy_ApiKey,
    "apiSecret": cfg.acct_cfg.coinigy_ApiSecret
}

var options = {
    hostname: "sc-02.coinigy.com",
    port: "443",
    secure: "true"
}

export function watchChannel(channelName: string, dataFunction: any) {
    var SCsocket = socketCluster.connect(options);

    SCsocket.on('connect', (status: any) => {
        SCsocket.on('error', (err: any) => {
            dataFunction(err, null);
            return;
        })

        SCsocket.emit("auth", apiCreds, (err: any, token: any) => {
            if(!err && token){
                var scChannel = SCsocket.subscribe(channelName);
                scChannel.watch((data: any) => {
                    dataFunction(null, data);
                });
            }
        })
    })
}