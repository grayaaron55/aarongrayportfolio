import * as request from 'request';
import { getConfig, Config, AcctConfig, DebugConfig, SwarmConfig, MarketConfig } from '../cfg';
import { PostCallFilter } from '../models/tools/api/PostCallFilter';
import { AccountInfo } from '../models/tools/api/dbmodels/AccountInfo';
import { ExchangeInfo } from '../models/tools/api/dbmodels/ExchangeInfo';

var cfg = getConfig();

export function GetMarketData(exchangeCode: string, exchangeMarket: string, type: string, callback: any){
    var marketFilter = new PostCallFilter(exchangeCode, exchangeMarket, type);

    MakeMarketDataCall(marketFilter, callback);
}

function MakeMarketDataCall(filter: PostCallFilter, callback: any){
    var callUrl = `https://api.coinigy.com/api/v1/data`;

    MakePostCall(callUrl, filter, (err: any, resp: any) => {
        if(err){
            callback(err, null);
            return;
        }

        callback(null, resp.data);
        return;
    });
}

function GetAccountData(callback: any){
    var callUrl = `https://api.coinigy.com/api/v1/accounts`;

    MakePostCall(callUrl, new PostCallFilter('','',''), (err: any, response: any) => {
        if(err){
            callback(err, null);
            return;
        }
        var data = response.data;
        callback(null, data);
    })
}

export function GetAccountByName(accountName: string, callback: any){
    GetAccountData((err: any, accounts: AccountInfo[]) => {
        if(err){
            callback(err, null);
            return;
        }

        for(var acct of accounts){
            if(acct.auth_nickname==accountName){
                callback(null, acct);
                return;
            }
        }

        callback(`Account not found`, null);
        return;
    })
}

export function GetExchangeDataById(exchangeId: number, callback: any){
    GetExchangeData((err: any, data: ExchangeInfo[]) => {
        if(err){
            callback(err, null);
            return;
        }

        for(var exch of data){
            if(exch.exchangeId==exchangeId){
                callback(null, exch);
                return;
            }
        }

        callback(`Exchange ${exchangeId} not found`, null);
        return;
    })
}

export function GetMarketsByExchange(exchangeCode: string, callback: any){
    GetMarketInfo(exchangeCode, (err: any, data: any) => {
        if(err){
            callback(err, null);
            return;
        }

        callback(null, data);
    })
}

export function GetMarketByName(marketName: string, exchangeCode: string, callback: any){
    GetMarketsByExchange(exchangeCode, (err: any, data: any) => {
        if(err){
            callback(err, null);
            return;
        }

        for(var market of data){
            if(market.mkt_name==marketName){
                callback(null, market);
                return;
            }
        }

        callback(`Market ${marketName} not found`, null);
    })
}

function GetExchangeData(callback: any){
    var callUrl = `${cfg.acct_cfg.coinigy_ApiUrl}/exchanges`;

    MakePostCall(callUrl, new PostCallFilter('','',''), (err: any, data: any) => {
        if(err){
            callback(err, null);
            return;
        }

        callback(null, data.data);
        return;
    })
}

export function GetMarketInfo(exchangeCode: string, callback: any){
    var callUrl = `${cfg.acct_cfg.coinigy_ApiUrl}/markets`;
    var callBody = {
        exchange_code: exchangeCode
    }
    MakePostCall(callUrl, callBody, (err: any, data: any) => {
        if(err){
            callback(err, null);
            return;
        }


        callback(null, data.data);
        return;
    })

}

function MakePostCall(callUrl: string, callBody: any, callback: any): void{
    // console.log(callBody);
    request({
        method: 'POST',
        url: callUrl,
        headers: {
            'Content-Type': 'application/json',
            'X-API-KEY': cfg.acct_cfg.coinigy_ApiKey,
            'X-API-SECRET': cfg.acct_cfg.coinigy_ApiSecret
        },
        body: JSON.stringify(callBody),
    }, 
    (err: any, response: any) => {
        if(err){
            callback(err, null);
            return;
        }
        
        try{
            // console.log(response.body);
            var parsed = JSON.parse(response.body); 
            callback(null, parsed);
            return;
        }
        catch{
            callback(`Error parsing JSON`, null);
            return;
        }
    });
}